using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerLightningAndTornado : MonoBehaviour
{
    private Vector3 originalPosition;
    [SerializeField] GameObject player;
    private void Start()
    {
        originalPosition = transform.localPosition;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "LightningBolt" || other.gameObject.name == "FirstTornado" || other.gameObject.name == "SecondTornado")
        {
            //player.GetComponent<CharacterController>().enabled = false;
            // player.transform.localPosition = originalPosition;
            // player.GetComponent<CharacterController>().enabled = true;
            player.GetComponent<Rigidbody>().AddForce(transform.up*5f,ForceMode.Impulse);
            player.GetComponent<Animator>().SetTrigger("fall");
            Debug.Log("udarac");
        }
    }
}
