using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class MonologueScriptHomeIslandStorm : MonoBehaviour
{

    private GameObject MonologuesAndHelpBox;
    private GameObject Screens;

    public GameObject textBox;
     AudioSource[] captionsSounds;
    private void Awake()
    {
        MonologuesAndHelpBox = GameObject.Find("Monologues and HelpBox");
        textBox = GameObject.Find("StormStartText");
        Screens = GameObject.Find("Screens");
        captionsSounds = GetComponents<AudioSource>();
    }

    private void Start()
    {
        if (!Screens.transform.GetChild(0).gameObject.activeSelf || !Screens.transform.GetChild(1).gameObject.activeSelf)
            StartCoroutine(showSubtitles());
    }
    IEnumerator showSubtitles()
    {
        yield return new WaitForSeconds(2);
        captionsSounds[0].Play(0);
        textBox.GetComponent<Text>().text = "Phlo: Oh no! This has never happened before! I must hurry to the tower or everything disappears in void!";
        yield return new WaitForSeconds(6);
        textBox.GetComponent<Text>().text = null;
        yield return new WaitForSeconds(8);
        captionsSounds[1].Play(0);
        textBox.GetComponent<Text>().text = "Phlo: It's really dangerous! I should try to keep away from tornadoes and lightning.. I think this wind with flying rocks doesn't help either..";
        yield return new WaitForSeconds(12);
        textBox.GetComponent<Text>().text = null;
    }
}
