using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerSphere : MonoBehaviour
{
    // private float speed = 2f;
    // private float height = 0.05f;
    // private float startY = 4.54f;
    // Update is called once per frame
    void Update()
    {
        // Levitate
        // var pos = transform.position;
        // var newY = startY + height*Mathf.Sin(Time.time * speed);
        // transform.position = new Vector3(pos.x, newY, pos.z);
    
        // Rotate
        transform.RotateAround(transform.position, transform.up, Time.deltaTime * 150f);
    }

}
