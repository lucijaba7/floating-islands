using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EndScene : MonoBehaviour
{
    [SerializeField] GameObject objectInHand;
    [SerializeField] GameObject ninaAndPhlo;
    [SerializeField] GameObject endBarrier;
    [SerializeField] GameObject tpCamera;
    [SerializeField] GameObject endCamera1;
    [SerializeField] GameObject endCamera2;
    [SerializeField] GameObject endIslandScreen;
    private GameObject nina;
    private GameObject phlo;
    private bool pickedUp = false;
    private GameObject captions; //Za prikazivanje teksta dijaloga
    AudioSource[] dialogueSounds; //Za puštanje zvukova dijaloga
    private bool moveForward = false;

    void Start()
    {
        nina = ninaAndPhlo.transform.GetChild(0).gameObject;
        phlo = ninaAndPhlo.transform.GetChild(1).gameObject;
        captions = GameObject.Find("Captions"); //Za prikazivanje teksta dijaloga
        dialogueSounds = GetComponents<AudioSource>();  //Za puštanje zvukova dijaloga
    }

    void Update()
    {
        if (moveForward)
        {
            tpCamera.transform.Translate(Vector3.forward * 0.0001f);
        }
    }

    public void pickUp(GameObject mObject)
    {
        if(!pickedUp)
        {
            pickedUp = true;
            transform.LookAt(mObject.transform.position);
            this.GetComponent<Animator>().SetTrigger("liftObject");
            StartCoroutine(putInHand());
            StartCoroutine(ninaAppear());
        }

    }

    private IEnumerator putInHand()
    {
        yield return new WaitForSeconds(1.4f);
        objectInHand.SetActive(true);
        yield return new WaitForSeconds(6);
        captions.GetComponent<Text>().text = "Oh no! How will I leave this island?!";
        dialogueSounds[0].Play(0); //Zvuk se nalazi na Characteru
        yield return new WaitForSeconds(3);
        captions.GetComponent<Text>().text = "";
    }

    private IEnumerator ninaAppear()
    {        
        yield return new WaitForSeconds(10);
        ninaAndPhlo.SetActive(true);
        yield return new WaitForSeconds(7);
        endBarrier.SetActive(true);
    }

    
    void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("end") )
        {
            StartCoroutine(finalInteraction());
        }   
    }

    private IEnumerator finalInteraction()
    {
        Debug.Log("final Interaction");
        PlayerFreeze();
        gameObject.transform.LookAt(new Vector3(ninaAndPhlo.transform.position.x, gameObject.transform.position.y, ninaAndPhlo.transform.position.z));
        tpCamera.transform.LookAt(ninaAndPhlo.transform.position);
        moveForward = true;
        yield return new WaitForSeconds(3);
        captions.GetComponent<Text>().text = "We meet again my old friend";
        dialogueSounds[1].Play(0);
        yield return new WaitForSeconds(3);
        moveForward = false;
        tpCamera.SetActive(false);
        endCamera1.SetActive(true);
        captions.GetComponent<Text>().text = "NINA! I'm so happy you are here! Can you give me a ride to my home island, I need to bring this madness to an end?";
        dialogueSounds[2].Play(0);
        yield return new WaitForSeconds(8);
        endCamera1.SetActive(false);
        endCamera2.SetActive(true);
        captions.GetComponent<Text>().text = "That's the least I can do after you made me beautiful again! Hold on tight, friend!";
        dialogueSounds[3].Play(0);
        yield return new WaitForSeconds(3);
        phlo.SetActive(true);
        captions.GetComponent<Text>().text = "";
        gameObject.transform.GetChild(0).gameObject.SetActive(false);
        yield return new WaitForSeconds(4);
        ninaAndPhlo.GetComponent<Animator>().SetTrigger("leave");
        yield return new WaitForSeconds(4.5f);
        endIslandScreen.SetActive(true);
        /// Prijelaz scene
    }

    private void PlayerFreeze()
    {
        gameObject.GetComponent<Animator>().SetFloat("Speed", 0);
        gameObject.GetComponent<BasicBehaviour>().enabled = false;
        tpCamera.GetComponent<ThirdPersonOrbitCamBasic>().enabled = false;
        gameObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
    }
}
