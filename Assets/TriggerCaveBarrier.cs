using UnityEngine;
using UnityEngine.SceneManagement;

public class TriggerCaveBarrier : MonoBehaviour
{
    GameObject CaveBarrier;
    void Start()
    {
        CaveBarrier = GameObject.Find("CaveBarrier");
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == CaveBarrier.name) SceneManager.LoadScene("Cave (model)");
    }
}
