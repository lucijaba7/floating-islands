using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TriggerEndGame : MonoBehaviour
{
    [SerializeField] GameObject character;
    [SerializeField] GameObject tpCamera;
    [SerializeField] GameObject sphere;
    [SerializeField] GameObject stand;
    [SerializeField] GameObject canvas;
    [SerializeField] GameObject objectFinal;
    [SerializeField] GameObject endCamera;
    [SerializeField] GameObject mObject;
    [SerializeField] GameObject explosion;
    [SerializeField] GameObject objectInHand;
    [SerializeField] GameObject storm;
    AudioSource[] captionsSounds;
    
    
    private bool moveCamera = false;
    private bool growSphere = false;
    
    

    void OnTriggerEnter(Collider other)
    {
        captionsSounds = GetComponents<AudioSource>();
        if(other.CompareTag("Player") )
        {
            Debug.Log("Player entered");
            StartCoroutine(EndGame());
        }
    }

    void Update()
    {
        if(moveCamera)
        {
            tpCamera.transform.LookAt(sphere.transform.position);
            tpCamera.transform.Translate(Vector3.left * 0.006f);
            
        }
        if(growSphere)
        {
            // Vector3 scale = new Vector3(sphere.transform.localScale.x + 0.03f, sphere.transform.localScale.y + 0.03f, sphere.transform.localScale.z + 0.03f);
            // sphere.transform.localScale = scale;

            Vector3 scale = new Vector3(mObject.transform.localScale.x + 0.02f, mObject.transform.localScale.y + 0.02f, mObject.transform.localScale.z + 0.02f);
            mObject.transform.localScale = scale;
            sphere.transform.localScale = scale;
        }

    }

    private IEnumerator EndGame()
    {
        captionsSounds[0].Play(0);
        moveCamera = true;
        character.GetComponent<Animator>().SetFloat("Speed", 0);
        character.GetComponent<MoveBehaviour>().enabled = false;
        character.GetComponent<BasicBehaviour>().enabled = false;
        tpCamera.GetComponent<ThirdPersonOrbitCamBasic>().enabled = false;
        character.transform.position = stand.transform.position;
        character.transform.rotation = stand.transform.rotation;
        character.GetComponent<Animator>().SetTrigger("putObject");
        yield return new WaitForSeconds(2);
        //Zvuk kada se postavi objekt na kulu
        captionsSounds[2].Play(0);
        objectInHand.SetActive(false);
        sphere.SetActive(true);
        mObject.SetActive(true);
        growSphere = true;
        yield return new WaitForSeconds(2.5f);
        // sphere.SetActive(false);
        // sphere.SetActive(true);
        // sphere.transform.localScale = new Vector3(0f,0f,0f);
        // yield return new WaitForSeconds(0.5f);
        // sphere.SetActive(false);
        // sphere.SetActive(true);
        // sphere.transform.localScale = new Vector3(0f,0f,0f);
        // yield return new WaitForSeconds(0.5f);
        // sphere.SetActive(false);
        // sphere.SetActive(true);
        // sphere.transform.localScale = new Vector3(0f,0f,0f);
        // yield return new WaitForSeconds(0.5f);
        // sphere.SetActive(false);
        // sphere.SetActive(true);
        // sphere.transform.localScale = new Vector3(0f,0f,0f);
        // yield return new WaitForSeconds(0.5f);
        // sphere.SetActive(false);
        // moveCamera = false;
        explosion.GetComponent<ParticleSystem>().Play();
        yield return new WaitForSeconds(0.5f);
        canvas.SetActive(true);
        yield return new WaitForSeconds(1.5f);
        // zvuk stvorio se
        sphere.SetActive(false);
        mObject.SetActive(false);
        character.SetActive(false);
        canvas.SetActive(false);
        storm.SetActive(false);
        objectFinal.SetActive(true);
        endCamera.SetActive(true);
        yield return new WaitForSeconds(4f);
        // kreće outro
        SceneManager.LoadScene("Outro");

    }
 
}
