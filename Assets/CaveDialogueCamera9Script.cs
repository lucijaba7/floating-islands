using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class CaveDialogueCamera9Script : MonoBehaviour
{
    [SerializeField] GameObject ninaWorminaButterfly;
    // Start is called before the first frame update
    void Start()
    {
        Debug.Log(gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        gameObject.transform.LookAt(ninaWorminaButterfly.transform.position);
        gameObject.transform.Translate(Vector3.left * 0.002f);
    }
}
