using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class IntroScenes : MonoBehaviour
{
    [SerializeField] private Material skyboxMaterial;
    [SerializeField] private GameObject camera1;
    [SerializeField] private GameObject camera2;
    [SerializeField] private GameObject camera3;
    [SerializeField] private GameObject camera4;
    [SerializeField] private GameObject camera5;
    [SerializeField] private GameObject camera6;
    [SerializeField] private GameObject camera7;
    [SerializeField] private GameObject camera8;
    [SerializeField] private GameObject movingPizzaCar;
    [SerializeField] private GameObject blackScreenCanvas;
    [SerializeField] private GameObject greenWorldHalf;
    [SerializeField] private GameObject worldExplosion;
    [SerializeField] private GameObject explosion01;
    [SerializeField] private GameObject babyAnimated2;
    [SerializeField] private GameObject grampaAnimated5;
    [SerializeField] private GameObject scene5Canvas;
    [SerializeField] private GameObject misteriozniObjekt;
    [SerializeField] private GameObject alien;
    [SerializeField] private GameObject grampaStandingAnimtion3;
    [SerializeField] private GameObject youngPhloWithAnimtion3;
    [SerializeField] private GameObject phlo;
    [SerializeField] private GameObject startScreen;
    [SerializeField] private GameObject bubblesIslandScreen;
    AudioSource[] introSounds;
    public GameObject introCaptions;
    //public GameObject blackScreenText;

    IEnumerator scene1(){
        Debug.Log("Scene 1 is playing");
        introSounds[0].Play(0);
        camera1.SetActive(true);
        movingPizzaCar.SetActive(true);
        yield return new WaitForSeconds(1); //Pauza prije pripovjedača
        introCaptions.GetComponent<Text>().text = "Once upon a time, there was a world bustling with life and life bustling with itself.";
        yield return new WaitForSeconds(5);
        introCaptions.SetActive(false);
        yield return new WaitForSeconds(1);//Vrijeme trajanja scene

        Debug.Log("Scene 2 is playing");
        introCaptions.GetComponent<Text>().text = " "; //Prazan tekst da ne stoji prijašnji tekst cijelo vrijeme
        blackScreenCanvas.SetActive(true); //Eableati canvas
        yield return new WaitForSeconds(1); //Pauza prije pripovjedača
        //blackScreenText.SetActive(true);
        introCaptions.SetActive(true);
        introCaptions.GetComponent<Text>().text = "Until one moment";
        introSounds[1].Play(0); //"Until one moment"
        RenderSettings.skybox = skyboxMaterial; //Promjena Skybox-a
        yield return new WaitForSeconds(3); //Vrijeme trajanja scene
        //blackScreenText.SetActive(false);
        introCaptions.GetComponent<Text>().text = " ";
        blackScreenCanvas.SetActive(false); //Isključiti canvas
        worldExplosion.SetActive(true);
        introSounds[2].Play(0); //Zvuk eksplozije
        explosion01.SetActive(true);
        camera2.SetActive(true);
        camera1.SetActive(false);
        yield return new WaitForSeconds(4.5f); //Vrijeme trajanja scene

        Debug.Log("Scene 3 is playing");
        greenWorldHalf.SetActive(true);
        camera3.SetActive(true);
        camera2.SetActive(false);
        yield return new WaitForSeconds(1); //Pauza prije pripovjedača
        introCaptions.GetComponent<Text>().text = "Half of life is completely gone, and the other half is all over the universe, flying into oblivion and its infinity.";
        introSounds[3].Play(0); //"Half of life is completely gone, and the other half is all over the universe, flying into oblivion and its infinity."
        yield return new WaitForSeconds(9);

        Debug.Log("Scene 4 is playing");
        introCaptions.GetComponent<Text>().text = " ";
        grampaAnimated5.SetActive(true);
        babyAnimated2.SetActive(true);
        camera3.SetActive(false);
        camera4.SetActive(true);
        yield return new WaitForSeconds(1); //Pauza prije pripovjedača
        introCaptions.GetComponent<Text>().text = "It's a tragic story that young Phlo heard from his grandfather.";
        introSounds[4].Play(0); //"It's a tragic story that young Dante heard from his grandfather."
        yield return new WaitForSeconds(5);

        Debug.Log("Scene 5 is playing");
        introCaptions.GetComponent<Text>().text = " ";
        camera4.SetActive(false);
        camera5.SetActive(true);
        scene5Canvas.SetActive(true);
        alien.SetActive(true);
        yield return new WaitForSeconds(1); //Pauza prije pripovjedača
        introCaptions.GetComponent<Text>().text = "Someone with an unclean heart has disturbed the stability of this peaceful world and brought perhaps irreparable consequences by stealing a mysterious object from the top of the tower.";
        introSounds[5].Play(0); //"Someone with an unclean heart has disturbed the stability of this peaceful world and brought perhaps irreparable consequences by stealing a mysterious object from the top of the tower."
        yield return new WaitForSeconds(5);
        misteriozniObjekt.SetActive(false);
        yield return new WaitForSeconds(10);

        Debug.Log("Scene 6 is playing");
        introCaptions.GetComponent<Text>().text = " ";
        scene5Canvas.SetActive(false); //Važno je isključiti canvas iz prethodne scene
        camera6.SetActive(true);
        camera5.SetActive(false);
        grampaStandingAnimtion3.SetActive(true);
        youngPhloWithAnimtion3.SetActive(true);
        yield return new WaitForSeconds(1); //Pauza prije pripovjedača
        introCaptions.GetComponent<Text>().text = "There is a theory that the floating islands can still be united by returning to the top of the tower what it lacks, so stability can be restored and perhaps the floating islands can be united into one world.";
        introSounds[6].Play(0); //"There is a theory that the floating islands can still be united by returning to the top of the tower what it lacks, so stability can be restored and perhaps the floating islands can be united into one world."
        yield return new WaitForSeconds(14);

        Debug.Log("Scene 7 is playing");
        introCaptions.GetComponent<Text>().text = " ";
        camera7.SetActive(true);
        camera6.SetActive(false);
        phlo.SetActive(true);
        yield return new WaitForSeconds(1); //Pauza prije pripovjedača
        introCaptions.GetComponent<Text>().text = "After a lot of effort and hard work. After countless mistakes he still hasn't given up. Phlo succeeded!";
        introSounds[7].Play(0); //"After a lot of effort and hard work. After countless mistakes he still hasn't given up. Phlo succeeded!"
        yield return new WaitForSeconds(10);

        Debug.Log("Scene 8 is playing");
        introCaptions.GetComponent<Text>().text = " ";
        camera8.SetActive(true);
        introSounds[9].Play(0); //Spacecraft
        yield return new WaitForSeconds(1); //Pauza prije pripovjedača
        introCaptions.GetComponent<Text>().text = "He will embark on an adventure that was only imaginable. All he knows is that 'Nina knows the way'";
        introSounds[8].Play(0); //"He will embark on an adventure that was only imaginable. All he knows is that “Nina knows the way”
        yield return new WaitForSeconds(8); //trajanje scene
        introCaptions.GetComponent<Text>().text = " ";
        camera7.SetActive(false);
        blackScreenCanvas.SetActive(true); //black screen
        yield return new WaitForSeconds(4); //trajanje black screen-a
        blackScreenCanvas.SetActive(false); //isključi black screen-a
        startScreen.SetActive(false);
        bubblesIslandScreen.SetActive(true);
    }
    
    void Start()
    {
        introCaptions = GameObject.Find("IntroCaptions");
        //blackScreenText = GameObject.Find("BlackScreenText");
        introSounds = GetComponents<AudioSource>();
        StartCoroutine(scene1()); //Pokretanje prve scene
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
