using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class startStorm : MonoBehaviour
{
    [SerializeField] GameObject lightning;
    [SerializeField] GameObject blackScreen;
    [SerializeField] GameObject darkScreen;
    [SerializeField] GameObject lightScreen;
    [SerializeField] GameObject player;
    [SerializeField] GameObject cameraAtTower;
    [SerializeField] GameObject cameraAtSky;
    [SerializeField] GameObject fallingPhlo;
    [SerializeField] GameObject bgMusic;
    AudioSource[] captionsSounds; //Za puštanje zvukova dijaloga
    public GameObject captions; 
    void OnTriggerEnter(Collider other)
    {
        captionsSounds = GetComponents<AudioSource>();
        captions = GameObject.Find("Captions");
        //start Nerr entrance animation/
        if(other.CompareTag("Player") )
        {
            Debug.Log("Start storm");
            captions.GetComponent<Text>().text = "Phlo: Ahh, finally...";
            captionsSounds[0].Play(0);
            /*
            Phlo kaze ovdje ah finally ///////////////////////////////////////////////
            */
            StartCoroutine(endEvent());
        }            
    }

    private IEnumerator endEvent()
    {
        yield return new WaitForSeconds(1);
        bgMusic.SetActive(false);
        /*
            
            Početak jakog vjetra i munje /////////////////////////////////////////////////////////////
        
        */
        lightning.SetActive(true);
        yield return new WaitForSeconds(3);
        captions.GetComponent<Text>().text = "";
        captionsSounds[1].Play(0);
        /*
            Zvuk munje /////////////////////////////////////////////////////////////
        */
        blackScreen.SetActive(true);

        player.SetActive(false);
        cameraAtTower.SetActive(true);

        yield return new WaitForSeconds(0.4f);
        blackScreen.SetActive(false);
        darkScreen.SetActive(true);
        yield return new WaitForSeconds(0.2f);
        darkScreen.SetActive(false);        
        lightScreen.SetActive(true);
        yield return new WaitForSeconds(0.3f);
        lightScreen.SetActive(false);        
        darkScreen.SetActive(true);
        yield return new WaitForSeconds(0.2f);
        darkScreen.SetActive(false);        
        lightScreen.SetActive(true);
        yield return new WaitForSeconds(0.3f);
        lightScreen.SetActive(false);      
        blackScreen.SetActive(true);
        yield return new WaitForSeconds(0.3f);
        blackScreen.SetActive(false);

        // gledamo tower
        yield return new WaitForSeconds(2f);
        captionsSounds[2].Play(0);
        /*
            
            Zvuk munje /////////////////////////////////////////////////////////////
            
            */
        blackScreen.SetActive(true);

        cameraAtTower.SetActive(false);
        cameraAtSky.SetActive(true);

        yield return new WaitForSeconds(0.3f);
        blackScreen.SetActive(false);
        darkScreen.SetActive(true);
        yield return new WaitForSeconds(0.1f);
        darkScreen.SetActive(false);        
        lightScreen.SetActive(true);
        yield return new WaitForSeconds(0.2f);
        lightScreen.SetActive(false);        
        darkScreen.SetActive(true);
        yield return new WaitForSeconds(0.1f);
        darkScreen.SetActive(false);        
        lightScreen.SetActive(true);
        yield return new WaitForSeconds(0.2f);
        lightScreen.SetActive(false);   
        blackScreen.SetActive(true);
        yield return new WaitForSeconds(0.2f);
        captionsSounds[3].Play(0); //Screaming sound
        cameraAtSky.SetActive(false);
        fallingPhlo.SetActive(true);
        
        blackScreen.SetActive(false);

        // cekamo kao anim
        yield return new WaitForSeconds(2f);
        captionsSounds[4].Play(0);
        /*
            
            Zvuk munje /////////////////////////////////////////////////////////////
            
            */
        blackScreen.SetActive(true);
        yield return new WaitForSeconds(0.3f);
        blackScreen.SetActive(false);
        darkScreen.SetActive(true);
        yield return new WaitForSeconds(0.1f);
        darkScreen.SetActive(false);        
        lightScreen.SetActive(true);
        yield return new WaitForSeconds(0.2f);
        
        lightScreen.SetActive(false);        
        darkScreen.SetActive(true);
        yield return new WaitForSeconds(0.1f);
        darkScreen.SetActive(false);        
        lightScreen.SetActive(true);
        yield return new WaitForSeconds(0.2f);
        lightScreen.SetActive(false);      
        blackScreen.SetActive(true);
        yield return new WaitForSeconds(0.2f);
        blackScreen.SetActive(false);
        
        yield return new WaitForSeconds(2.7f);
        blackScreen.SetActive(true);

        yield return new WaitForSeconds(1.5f);
        SceneManager.LoadScene("Home Island - Storm");
        // yield return new WaitForSeconds(0.4f);
        // blackScreen.SetActive(false);
        // darkScreen.SetActive(true);
        // yield return new WaitForSeconds(0.2f);
        // darkScreen.SetActive(false);        
        // lightScreen.SetActive(true);
        // yield return new WaitForSeconds(0.3f);
        // lightScreen.SetActive(false);        
        // darkScreen.SetActive(true);
        // yield return new WaitForSeconds(0.2f);
        // darkScreen.SetActive(false);        
        // lightScreen.SetActive(true);
        // yield return new WaitForSeconds(0.3f);
        // lightScreen.SetActive(false);      
        // blackScreen.SetActive(true);
        // yield return new WaitForSeconds(0.3f);
        // blackScreen.SetActive(false);
    }

}
