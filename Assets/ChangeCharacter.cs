using System.Collections;
using UnityEngine;
using System;
using UnityEngine.UI;

public class ChangeCharacter : MonoBehaviour
{
    private GameObject SlimeCounterText;
    private GameObject Character;
    [SerializeField] Animator characterAnimator;
    private Transform firstChild;
    private Transform secondChild;
    private Transform thirdChild;
    private Transform fourthChild;
    private Transform Explosion;
    private GameObject RadioactiveRain;
    private GameObject MonologueText;
    private string text;
    private int slimeCounter;
    private bool firstTimeOnFirstNumOfSlimes = true;
    private bool firstTimeOnSecondNumOfSlimes = true;
    private bool firstTimeOnThirdNumOfSlimes = true;
    AudioSource[] dialogueSounds;
    [SerializeField] GameObject slimes;
    [SerializeField] GameObject rain;


    void Start()
    {
        dialogueSounds = GetComponents<AudioSource>();

        SlimeCounterText = GameObject.Find("SlimeCounterText");
        Character = GameObject.Find("Phlo - Character and Movement");
        RadioactiveRain = GameObject.Find("RadioactiveRain");
        MonologueText = GameObject.Find("MonologueText");
        firstChild = Character.transform.GetChild(0);
        secondChild = Character.transform.GetChild(1);
        thirdChild = Character.transform.GetChild(2);
        fourthChild = Character.transform.GetChild(3);

        Explosion = Character.transform.GetChild(Character.transform.childCount - 1);
        RadioactiveRain.GetComponent<ParticleSystem>().Play();
        RadioactiveRain.GetComponent<AudioSource>().Play();
        StartCoroutine(showSubtitlesFirstRain());
    }


    void Update()
    {
        text = SlimeCounterText.GetComponent<Text>().text;
        slimeCounter = int.Parse(text.Substring(18));
        if(slimeCounter < 290 && (RadioactiveRain.GetComponent<AudioSource>().isPlaying
            || RadioactiveRain.GetComponent<ParticleSystem>().isPlaying))
        {
            RadioactiveRain.GetComponent<ParticleSystem>().Stop();
            RadioactiveRain.GetComponent<AudioSource>().Stop();
        }
        else if(slimeCounter == 210 || slimeCounter == 110 || slimeCounter == 10)
        {
            RadioactiveRain.GetComponent<ParticleSystem>().Play();
            RadioactiveRain.GetComponent<AudioSource>().Play();
        }
        else if((slimeCounter < 210 && slimeCounter > 200 || slimeCounter < 110 && slimeCounter > 100 || slimeCounter < 10 && slimeCounter > 0)
            && (!RadioactiveRain.GetComponent<AudioSource>().isPlaying 
            || !RadioactiveRain.GetComponent<ParticleSystem>().isPlaying))
        {
            RadioactiveRain.GetComponent<ParticleSystem>().Play();
            RadioactiveRain.GetComponent<AudioSource>().Play();
        }
        else if (slimeCounter <= 200 && slimeCounter > 100
            && firstTimeOnFirstNumOfSlimes)
        {
            setPosition(firstChild, secondChild);
            Explosion.GetComponent<ParticleSystem>().Play();
            RadioactiveRain.GetComponent<ParticleSystem>().Stop();
            RadioactiveRain.GetComponent<AudioSource>().Stop();
            firstChild.gameObject.SetActive(false); //Deaktivira avatara s velikim rukama
            secondChild.gameObject.SetActive(true); //Aktivira avatara sfere
            firstTimeOnFirstNumOfSlimes = false;
            StartCoroutine(showSubtitlesSecondRain());

        }
        else if (slimeCounter <= 100 && slimeCounter > 0
            && firstTimeOnSecondNumOfSlimes)
        {
            setPosition(secondChild, thirdChild);
            Explosion.GetComponent<ParticleSystem>().Play();
            RadioactiveRain.GetComponent<ParticleSystem>().Stop();
            RadioactiveRain.GetComponent<AudioSource>().Stop();
            secondChild.gameObject.SetActive(false);
            thirdChild.gameObject.SetActive(true);
            firstTimeOnSecondNumOfSlimes = false;
            thirdChild.GetComponent<MoveBehaviour>().jumpHeight = 5;
            thirdChild.GetComponent<MoveBehaviour>().jumpIntertialForce = 10;
            StartCoroutine(showSubtitlesThirdRain());
        }
        else if (slimeCounter <= 0
            && firstTimeOnThirdNumOfSlimes)
        {
            setPosition(thirdChild, fourthChild);
            Explosion.GetComponent<ParticleSystem>().Play();
            RadioactiveRain.GetComponent<ParticleSystem>().Stop();
            RadioactiveRain.GetComponent<AudioSource>().Stop();
            thirdChild.gameObject.SetActive(false);
            fourthChild.gameObject.SetActive(true);
            firstTimeOnThirdNumOfSlimes = false;
            fourthChild.GetComponent<MoveBehaviour>().jumpHeight = 1.5f;
            fourthChild.GetComponent<MoveBehaviour>().jumpIntertialForce = 2;
            slimes.SetActive(false);
            rain.SetActive(false);
        }
    }
    IEnumerator showSubtitlesFirstRain()
    {
        yield return new WaitForSeconds(3);
        MonologueText.GetComponent<Text>().text = "What is this thing ? I can feel it.";
        dialogueSounds[0].Play(0);
        yield return new WaitForSeconds(3);
        MonologueText.GetComponent<Text>().text = "I only have a few minutes until this rain kills me.";
        dialogueSounds[1].Play(0);
        yield return new WaitForSeconds(3);
        MonologueText.GetComponent<Text>().text = null;
    }
    IEnumerator showSubtitlesSecondRain()
    {
        yield return new WaitForSeconds(0);
        MonologueText.GetComponent<Text>().text = "A ball!? It must be the radioactive rain that's causing this'";
        dialogueSounds[2].Play(0);
        yield return new WaitForSeconds(5);
        MonologueText.GetComponent<Text>().text = null;
    }
    IEnumerator showSubtitlesThirdRain()
    {
        yield return new WaitForSeconds(0);
        MonologueText.GetComponent<Text>().text = "I feel like Bigfoot now. I need to stop this rain as soon as possible.";
        dialogueSounds[3].Play(0);
        yield return new WaitForSeconds(5);
        MonologueText.GetComponent<Text>().text = null;
    }

    private void setPosition(Transform oldGameobject, Transform newGameobject) {
        newGameobject.position = oldGameobject.position;
        Explosion.transform.position = oldGameobject.position;
    }
}
