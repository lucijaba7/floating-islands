using UnityEngine;
using UnityEngine.SceneManagement;

public class TriggerTowerBarrier : MonoBehaviour
{
    GameObject TowerBarrier;
    void Start()
    {
        TowerBarrier = GameObject.Find("TowerBarrier");
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == TowerBarrier.name) SceneManager.LoadScene("(TODO) Home Island - Storm");
    }
}
