using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;

public class RiseObjectsAfterRadioactiveRain : MonoBehaviour
{
    private float nextActionTime = 0.0f;
    public float period = 0.1f;
    private GameObject ObjectsToRise;
    private GameObject SlimeCounterText;
    private GameObject RadioactiveIsland;
    private GameObject treestanLud;
    private GameObject character;
    private string text;
    private int slimeCounter;
    private Vector3 position;
    private Vector3 positionTreestanLud;
    private bool endOfEnemies = false;
    private Animator treestanAnimator;

    void Start()
    {
        SlimeCounterText = GameObject.Find("SlimeCounterText");
        RadioactiveIsland = GameObject.Find("Radioactive Island");
    }

    void Update()
    {
        text = SlimeCounterText.GetComponent<Text>().text;
        slimeCounter = int.Parse(text.Substring(18));
        if (slimeCounter <= 0) endOfEnemies = true; 
        if (endOfEnemies) 
        {
            enableAfterRadioactiveRainWorld();
            //characterLookAtTreestan();
            if (Time.time > nextActionTime && (ObjectsToRise.transform.position.y < 0))
            {
                position = ObjectsToRise.transform.position;
                nextActionTime += period;
                position.y += 0.05f;
                ObjectsToRise.transform.position = position;
            }
        }
    }
    void enableAfterRadioactiveRainWorld()
    {
        RadioactiveIsland.transform.GetChild(1).gameObject.SetActive(false);
        RadioactiveIsland.transform.GetChild(0).gameObject.SetActive(true);
        treestanLud = GameObject.Find("TreestanLud");
        positionTreestanLud = treestanLud.transform.position;
        positionTreestanLud.y = -7;
        treestanLud.transform.position = positionTreestanLud;
        ObjectsToRise = GameObject.Find("ObjectsToRise");
        treestanAnimator = GameObject.Find("TreestanLud").GetComponent<Animator>();
        treestanAnimator.SetBool("ActivateRise", true);
    }
    void characterLookAtTreestan()
    {
        treestanLud = GameObject.Find("TreestanLud");
        character = GameObject.Find("Character");
        character.transform.LookAt(treestanLud.transform.position);
    }
}
