using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MakeHittingOnlyOnce : MonoBehaviour
{
    void setHittingBoolFalse()
    {
        gameObject.GetComponent<Animator>().SetBool("hitRight", false);
        gameObject.GetComponent<Animator>().SetBool("hitLeft", false);
    }
}
