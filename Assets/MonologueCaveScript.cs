using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class MonologueCaveScript : MonoBehaviour
{
    public GameObject caveText;
    private void Awake()
    {
        caveText = GameObject.Find("CaveText");
    }

    private void Start() { 
        dialogueSounds = GetComponents<AudioSource>();
        StartCoroutine(showSubtitles());
        }
    AudioSource[] dialogueSounds;
    IEnumerator showSubtitles()
    {
        yield return new WaitForSeconds(3);
        caveText.GetComponent<Text>().text = "Phlo: I have to find Nina";
        dialogueSounds[0].Play(0);
        yield return new WaitForSeconds(5);
        caveText.GetComponent<Text>().text = "";
    }
}
