using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ChangeSceneInCave : MonoBehaviour
{
    [SerializeField] GameObject startScreen;
    [SerializeField] Slider loading;
    private void Update()
    {
        if (gameObject.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("StandFlyAwayCamera"))
        {
            //StartCoroutine(startRadioactiveIsland());
            SceneManager.LoadScene("Unstable Island");

        }
    }

    private IEnumerator startRadioactiveIsland()
    {
        startScreen.SetActive(true);
        yield return new WaitForSeconds(2f);
        loading.value = 0.2f;
        yield return new WaitForSeconds(2f);
        loading.value = 0.6f;
        yield return new WaitForSeconds(2f);
        loading.value = 1f;
        yield return new WaitForSeconds(1f);
        startScreen.SetActive(false);
    }
}
