using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerRock : MonoBehaviour
{
    private Vector3 originalPosition;
    [SerializeField] GameObject player;
    private GameObject Rocks;
    private void Start()
    {
        originalPosition = transform.localPosition;
        Rocks = GameObject.Find("Rocks");
    }
    private void OnTriggerEnter(Collider other)
    {
        for (int i = 0; i < Rocks.transform.childCount; i++)
        {
            if (other.gameObject.name == Rocks.transform.GetChild(i).name)
            {
                player.GetComponent<Animator>().SetTrigger("die");
                Debug.Log("smrt");
                StartCoroutine(die());
            }
        }
    } 
    IEnumerator die()
    {   
        yield return new WaitForSeconds(3);
        player.transform.localPosition = originalPosition;
        player.GetComponent<Animator>().SetTrigger("alive");
    }
}
