using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SphereController : MonoBehaviour
{
    public Rigidbody rb; 
    public float moveSpeed = 10f;
    public float jumpPower = 0.001f;
    
    private bool isGrounded;

    private float xInput;
    private bool yInput;
    private float zInput;
    

    // Start is called before the first frame update
    // Awake se pozove dok se igra load-a
    void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }


    // Update is called once per frame
    // Za input processing i animacije bolje
    void Update()
    {
        ProcessInputs();
    }
    
    // Zove se konzistetno (konsistent rate)
    // Za fiziku bolje
    void FixedUpdate()
    {
        // Movement
        Move();
        Jump();
       
    }

    private void ProcessInputs()
    {
        xInput = Input.GetAxis("Horizontal");
        yInput = Input.GetButton("Jump");
        zInput = Input.GetAxis("Vertical");
    }

    private void Move()
    {
        rb.AddForce(new Vector3(xInput, 0f, zInput) * moveSpeed);
    }

    private void Jump()
    {
        isGrounded = Physics.Raycast(transform.position, Vector3.down, 0.2f);
        if (yInput && isGrounded)
        {
            rb.AddForce(Vector3.up * jumpPower, ForceMode.Impulse);
        }
    }

}
