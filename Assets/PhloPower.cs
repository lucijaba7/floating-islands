using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PhloPower : MonoBehaviour
{
    public Slider slider;

    public void SetMaxPower(int power)
    {
        slider.maxValue = power;
        slider.value = power;
    }

    public void SetPower(int power)
    {
        slider.value = power;
    }

    public void FillMagazine(int power)
    {
        StartCoroutine(FillSlowly(power));
    }

    private IEnumerator FillSlowly(int targetValue)
    {

        float currentValue = slider.value;
        while(currentValue <= targetValue)
        {
            slider.value = currentValue;
            currentValue += 10;

            yield return new WaitForSeconds(0.001f);
        }
        yield return null;
    }
}
