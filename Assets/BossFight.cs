using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BossFight : MonoBehaviour
{
    [SerializeField] GameObject nerr;
    [SerializeField] GameObject phlo;
    [SerializeField] public GameObject healthCanvas;
    [SerializeField] public GameObject powerCanvas;
    [SerializeField] public SphereSpawnerController sphereSpawner;
    [SerializeField] GameObject projectile;
    private bool phloShootEnabled = false;
    [SerializeField] BossEnemy bossEnemy;
    [SerializeField] TakeDamage takeDamage;
    [SerializeField] PhloShoot phloShoot;
    [SerializeField] GameObject spheres;
    

    public void ResetBossFight()
    {
        Debug.Log("RESET BOSS");
        nerr.transform.position = new Vector3(17f, 3.994892f, -29f);
        phlo.GetComponent<Animator>().SetTrigger("alive");
        phlo.transform.position = new Vector3(-0.9f, 7f, 0.3f);

        bossEnemy.resetNerrHealth();  
        takeDamage.resetCharacter();  
        phloShoot.resetPowerToZero(); 
    }

    public void StartBossFight()
    {
        nerr.GetComponent<Animator>().SetBool("BossFightStart", true);
        nerr.GetComponent<BossEnemy>().enabled = true;
        healthCanvas.SetActive(true);
        sphereSpawner.triggerStart();
    }

    public void togglePhloShoot()
    {
        phloShootEnabled = !phloShootEnabled;
        Debug.Log("TOGGLE PHLO SHOOT");
        phlo.GetComponent<AimBehaviourBasic>().enabled = phloShootEnabled;
        projectile.SetActive(phloShootEnabled);
    }

    public void EndBossFight()
    {
        togglePhloShoot();
        healthCanvas.SetActive(false);
        powerCanvas.SetActive(false);
        spheres.SetActive(false);
    }
}
