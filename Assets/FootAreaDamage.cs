using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FootAreaDamage : MonoBehaviour
{
    [SerializeField] Animator characterAnimator;
    [SerializeField] GameObject leftFoot;
    [SerializeField] GameObject rightFoot;
    [SerializeField] GameObject particle;
    private bool areaDamage = false;
    

    void Update()
    {
        if(areaDamage)
        {
            areaDamage = false;
            StartCoroutine(activateAreaAttack());

        }
    }

    private IEnumerator activateAreaAttack()
    {
        yield return new WaitForSeconds(2f);
        leftFoot.GetComponent<CapsuleCollider>().enabled = true;
        rightFoot.GetComponent<CapsuleCollider>().enabled = true;
        
        Instantiate(particle, transform.position, Quaternion.identity);
        // Zvuk udarca u pod
        yield return new WaitForSeconds(1f);
        leftFoot.GetComponent<CapsuleCollider>().enabled = false;
        rightFoot.GetComponent<CapsuleCollider>().enabled = false;
    }
private void OnTriggerExit(Collider other)
{
    
    if (other.CompareTag("Ground"))
    {
        areaDamage = true;
    }
}

}
