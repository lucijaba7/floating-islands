using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class TriggerOpenShelter : MonoBehaviour
{
    [SerializeField] public Camera caveDialogueCamera1;
    [SerializeField] public Camera caveDialogueCamera2;
    [SerializeField] public Camera caveDialogueCamera3;
    [SerializeField] public Camera caveDialogueCamera4;
    [SerializeField] public Camera caveDialogueCamera5;
    [SerializeField] public Camera caveDialogueCamera6;
    [SerializeField] public Camera caveDialogueCamera7;
    [SerializeField] public Camera caveDialogueCamera8;
    [SerializeField] public Camera caveDialogueCamera9;
    [SerializeField] public Camera caveDialogueCamera10;
    [SerializeField] GameObject ninaAndPhlo; 
    private GameObject openShelterZone;
    private GameObject cage;
    private GameObject missNinaWorminaContainer;
    private GameObject missNinaWormina;
    private GameObject missNinaWorminaButterfly;
    private GameObject exitCaveTarget;
    private GameObject newSceneTransition;
    private GameObject flyAway;
    private Transform explosion;
    private Animator cageAnimator;
    private Animator characterAnimator;
    private bool inZone = false;
    private bool initiatedMonologue = false;
    private bool exitCaveBarrier = false;
    private bool flyAwayStarted = false;
    private GameObject caveText;
    private GameObject helpText;
    private GameObject caveDialogue;
    private GameObject character;
    private Camera mainCamera;
    private Camera dialogueCamera;
    private Vector3 position;
    AudioSource[] dialogueSounds;
    void Start()
    {
        dialogueSounds = GetComponents<AudioSource>();

        openShelterZone = GameObject.Find("OpenShelterZone");
        cage = GameObject.Find("Cage");
        missNinaWorminaContainer = GameObject.Find("Miss Nina Wormina");
        missNinaWormina = GameObject.Find("Nina_Wormina");
        helpText = GameObject.Find("HelpText");
        caveText = GameObject.Find("CaveText");
        exitCaveTarget = GameObject.Find("ExitCaveTarget");
        caveDialogue = GameObject.Find("CaveDialogue");
        newSceneTransition = GameObject.Find("NewSceneTransition");
        cageAnimator = cage.GetComponent<Animator>();
        character = GameObject.Find("Character");
        characterAnimator = character.GetComponent<Animator>();
        mainCamera = Camera.main;
        dialogueCamera = GameObject.Find("DialogueCamera").GetComponent<Camera>();
        explosion = missNinaWorminaContainer.transform.GetChild(missNinaWorminaContainer.transform.childCount - 1);
        flyAway = GameObject.Find("FlyAway");
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == openShelterZone.name) inZone = true;
    }
    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.name == openShelterZone.name) inZone = false;
    }
    private void Update()
    {
        if (exitCaveBarrier) missNinaWormina.transform.position = Vector3.MoveTowards(missNinaWormina.transform.position, exitCaveTarget.transform.position, 0.02f);
        if (flyAwayStarted)
        {
            missNinaWorminaButterfly.transform.position = Vector3.MoveTowards(missNinaWorminaButterfly.transform.position, newSceneTransition.transform.position, 0.005f);
            character.transform.position = Vector3.MoveTowards(character.transform.position, newSceneTransition.transform.position, 0.005f);
        }
        if (Input.GetButtonDown("Fire1") && inZone
            && !isPlaying(cageAnimator, "Open")
            && !initiatedMonologue
            && caveText.GetComponent<Text>().text == "")
        {
            cageAnimator.SetTrigger("Active");
            initiatedMonologue = true;
            exitCaveBarrier = true;
            StartCoroutine(caveDialogues());
        }

        if (inZone && !initiatedMonologue && caveText.GetComponent<Text>().text == "") { helpText.GetComponent<Text>().text = "Press 'F' to open"; }
        else helpText.GetComponent<Text>().text = null;
    }

    bool isPlaying(Animator anim, string stateName)
    {
        int animLayer = 0;

        if (anim.GetCurrentAnimatorStateInfo(animLayer).IsName(stateName) &&
                anim.GetCurrentAnimatorStateInfo(animLayer).normalizedTime < 1.0f)
            return true;
        else
            return false;
    }
    IEnumerator caveDialogues()
    {
        freezePlayer();
        activateDialogueCamera(true);
        yield return new WaitForSeconds(3); //Čekanje 3 sekunde
        //Početak razgovora između Phlo-a i Gospođe Nine Wormine
        caveDialogue.GetComponent<Text>().text = "Miss Nina Wormina: WHO ARE YOU?! WHAT DO YOU WANT?!";
        dialogueSounds[0].Play(0);
        yield return new WaitForSeconds(4); //Trajanje audio klipa iznad
        caveDialogue.GetComponent<Text>().text = "Phlo: I'm sorry, are you miss Nina?";
        dialogueSounds[1].Play(0);
        yield return new WaitForSeconds(3);
        
        caveDialogueCamera1.gameObject.SetActive(true); //Kamera gleda Ninu Worminu /////////
        dialogueCamera.gameObject.SetActive(false); //Isključujemo glavnu kameru /////////
        Debug.Log("caveDialogueCamera1 je aktivna");
        caveDialogue.GetComponent<Text>().text = "Miss Nina Wormina: Yes I am Nina Wormina, who wants to know?!";
        dialogueSounds[2].Play(0);
        yield return new WaitForSeconds(4);
        
        caveDialogueCamera2.gameObject.SetActive(true); //Uključujemo sljedeću kameru /////////
        caveDialogueCamera1.gameObject.SetActive(false); //Isključujemo prijašnju kameru ////////
        Debug.Log("caveDialogueCamera2 je aktivna");
        caveDialogue.GetComponent<Text>().text = "Phlo: My name is Phlo, my grandfather told me to seek you and ask you to help me. I have come here to try and change this, save our world once and for all. Connect our worlds back together.";
        dialogueSounds[3].Play(0);
        yield return new WaitForSeconds(12);
        
        caveDialogueCamera1.gameObject.SetActive(true);
        caveDialogueCamera2.gameObject.SetActive(false);
        Debug.Log("caveDialogueCamera1 je aktivna");
        caveDialogue.GetComponent<Text>().text = "Miss Nina Wormina: Are you... are you one of Ticco's family?";
        dialogueSounds[4].Play(0);
        yield return new WaitForSeconds(4);
        
        caveDialogueCamera2.gameObject.SetActive(true);
        caveDialogueCamera1.gameObject.SetActive(false);
        Debug.Log("caveDialogueCamera2 je aktivna");
        caveDialogue.GetComponent<Text>().text = "Phlo: I am, miss, I'm his grandson.";
        dialogueSounds[5].Play(0);
        yield return new WaitForSeconds(3);
        
        caveDialogueCamera3.gameObject.SetActive(true);
        caveDialogueCamera2.gameObject.SetActive(false);
        Debug.Log("caveDialogueCamera3 je aktivna");
        caveDialogue.GetComponent<Text>().text = "Miss Nina Wormina: Oh.. I have known your grandfather since he was little, he helped me a lot even when I got cursed and became this thing. I wasn't always Nina Wormina, I was only Nina, everyone knew me.. everyone thought I was the prettiest in all realms but look at me now.. disgusting..";
        dialogueSounds[6].Play(0);
        yield return new WaitForSeconds(18);
        
        caveDialogue.GetComponent<Text>().text = "Phlo: Why were you cursed?";
        caveDialogueCamera4.gameObject.SetActive(true);
        caveDialogueCamera3.gameObject.SetActive(false);
        Debug.Log("caveDialogueCamera4 je aktivna");
        dialogueSounds[7].Play(0);
        yield return new WaitForSeconds(3);
        
        caveDialogueCamera5.gameObject.SetActive(true);
        caveDialogueCamera4.gameObject.SetActive(false);
        Debug.Log("caveDialogueCamera5 je aktivna");
        caveDialogue.GetComponent<Text>().text = "Miss Nina Wormina: I was cursed in an act of brutality.. Nerr has cursed me because I was friends with your grandfather and thought I would help him with connecting islands back together..";
        dialogueSounds[8].Play(0);
        yield return new WaitForSeconds(10);
        
        caveDialogueCamera6.gameObject.SetActive(true);
        caveDialogueCamera5.gameObject.SetActive(false);
        Debug.Log("caveDialogueCamera6 je aktivna");
        caveDialogue.GetComponent<Text>().text = "Phlo: Can I somehow help you, miss?";
        dialogueSounds[9].Play(0);
        yield return new WaitForSeconds(3);
        
        caveDialogueCamera7.gameObject.SetActive(true);
        caveDialogueCamera6.gameObject.SetActive(false);
        Debug.Log("caveDialogueCamera6 je aktivna");
        caveDialogue.GetComponent<Text>().text = "Miss Nina Wormina: Your help has already begun, I can feel it, my curse can only be lifted if I can help you. I know what you want and need to do, you'll follow your grandfathers plan.";
        dialogueSounds[10].Play(0);
        yield return new WaitForSeconds(11);

        caveDialogueCamera8.gameObject.SetActive(true);
        caveDialogueCamera7.gameObject.SetActive(false);
        Debug.Log("caveDialogueCamera8 je aktivna");
        missNinaWorminaContainer.transform.GetChild(0).gameObject.SetActive(false);
        explosion.GetComponent<ParticleSystem>().Play();
        missNinaWorminaContainer.transform.GetChild(1).gameObject.SetActive(true);
        missNinaWorminaButterfly = GameObject.Find("Nina_Butterfly");
        caveDialogue.GetComponent<Text>().text = "Phlo: Wow!";
        dialogueSounds[11].Play(0);
        yield return new WaitForSeconds(2);

        caveDialogueCamera9.gameObject.SetActive(true);
        caveDialogueCamera8.gameObject.SetActive(false);
        Debug.Log("caveDialogueCamera9 je aktivna");
        caveDialogue.GetComponent<Text>().text = "Miss Nina Wormina: OH! Look at me.. Look at my wings.. It's happening, I'm me again, I can stop hiding! Come, I'll help you get to where you need to go, you should try to find Treestan he will help you from this point on! Hold on tight! ";
        dialogueSounds[12].Play(0);
        yield return new WaitForSeconds(16);
        caveDialogue.GetComponent<Text>().text = null;

        
        ninaAndPhlo.SetActive(true);
        missNinaWorminaButterfly.SetActive(false);
        character.SetActive(false);
        caveDialogueCamera10.gameObject.SetActive(true);
        caveDialogueCamera9.gameObject.SetActive(false);
        Debug.Log("caveDialogueCamera10 je aktivna");
        




        // setForFlyaway();
    }
    void freezePlayer()
    {
        character.GetComponent<BasicBehaviour>().enabled = false;
        character.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionY;
        characterAnimator.SetBool("FlyIdle", true);
        character.transform.LookAt(missNinaWormina.transform);
    }
    void activateDialogueCamera(bool dialogueCameraActive)
    {
        mainCamera.enabled = !dialogueCameraActive;
        dialogueCamera.enabled = dialogueCameraActive;
    }
}
