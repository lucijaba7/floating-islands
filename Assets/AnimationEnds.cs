using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationEnds : StateMachineBehaviour
{
    private GameObject FloatingTreeLogs;
    private GameObject CharacterAndMovement;
    private GameObject PlayabilityElements;

    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        FloatingTreeLogs = GameObject.Find("FloatingTreeLogs");
        CharacterAndMovement = GameObject.Find("Character and Movement");
        PlayabilityElements = GameObject.Find("Playability Elements");
    }

    //OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        CharacterAndMovement.transform.parent = PlayabilityElements.transform;
    }
}
