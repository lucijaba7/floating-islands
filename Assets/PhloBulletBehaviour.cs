using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhloBulletBehaviour : MonoBehaviour
{
    [SerializeField] GameObject particle;

    void OnCollisionEnter(Collision co)
    {
        if(co.collider.tag == "Nerr")
        {
            Instantiate(particle, gameObject.transform.position, Quaternion.identity);
            Destroy(gameObject);
            // newParticle.transform.localScale = sizeOfParticle;
            // StartCoroutine(PhysicalAttack());
            // Zvuk il sta ja znam
        }
        if(co.collider.tag == "worldBarrier")
            Destroy(gameObject);
            
    }

}
