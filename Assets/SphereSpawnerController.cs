using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SphereSpawnerController : MonoBehaviour
{
     public GameObject captions; //Za prikazivanje teksta dijaloga
     AudioSource[] dialogueSounds; //Za puštanje zvukova dijaloga
    public GameObject[] spheres;
    [SerializeField] GameObject sphereCamera;
    [SerializeField] GameObject helpBox;
    
    // Start is called before the first frame update
    void Start()
    {
        deactivateAll();
        captions = GameObject.Find("Captions"); //Za prikazivanje teksta dijaloga
        dialogueSounds = GetComponents<AudioSource>();  //Za puštanje zvukova dijaloga
        //helpBox = GameObject.Find("Monologues and HelpBox");
    }

    public void deactivateAll()
    {
        for (int i = 0; i < 4; i++)
        {
            spheres[i].SetActive(false);
        }
    }

    public void triggerStart()
    {
        StartCoroutine(startSphere());
    }

    private IEnumerator startSphere()
    {
        yield return new WaitForSeconds(12);
        dialogueSounds[0].Play(0);
        spheres[0].SetActive(true);
        sphereCamera.SetActive(true);

        captions.GetComponent<Text>().text = "What's that?! Is that the legendary magic my grandfather told me that will help me?";
        dialogueSounds[1].Play(0);
        yield return new WaitForSeconds(5);
        captions.GetComponent<Text>().text = "I think it is, I need to absorb it fast!";
        dialogueSounds[2].Play(0);
        helpBox.SetActive(true);
        sphereCamera.SetActive(false); 
        yield return new WaitForSeconds(4);
        captions.GetComponent<Text>().text = "";

    }

    public void triggerChangeSphere(int recentlyActiveSphere)
    {
        StartCoroutine(changeSphere(recentlyActiveSphere));
    }

    private IEnumerator changeSphere(int recentlyActiveSphere)
    {
        yield return new WaitForSeconds(1);
        spheres[recentlyActiveSphere].SetActive(false);
        // zvuk nestajanja
        yield return new WaitForSeconds(5);
        dialogueSounds[0].Play(0);
        int oppositeSphere = (recentlyActiveSphere += 2) > 3 ? recentlyActiveSphere - 4 : recentlyActiveSphere;
        spheres[oppositeSphere].SetActive(true);
    }
}
