using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class StartHomeIsland : MonoBehaviour
{
    [SerializeField] GameObject startScreen;
    [SerializeField] Slider loading;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(startHomeIsland());
    }

    private IEnumerator startHomeIsland()
    {
        startScreen.SetActive(true);
        yield return new WaitForSeconds(2f);
        loading.value = 0.2f;
        yield return new WaitForSeconds(2f);
        loading.value = 0.6f;
        yield return new WaitForSeconds(2f);
        loading.value = 1f;
        SceneManager.LoadScene("Home Island 1");
        yield return new WaitForSeconds(0.1f);
        startScreen.SetActive(false);
        
    }

}
