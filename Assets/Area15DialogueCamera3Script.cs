using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Area15DialogueCamera3Script : MonoBehaviour
{
    [SerializeField] GameObject nerr;
    // Start is called before the first frame update
    void Start()
    {
        Debug.Log(gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        gameObject.transform.LookAt(nerr.transform.position);
        gameObject.transform.Translate(Vector3.left * 0.003f);
    }
}

