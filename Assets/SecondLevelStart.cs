using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SecondLevelStart : MonoBehaviour
{
    private int groundedBool;
    public GameObject player;
    private GameObject monologueText;
    [SerializeField] FlyBehaviour flyBehaviour;
    AudioSource[] dialogueSounds;
    [SerializeField] Camera mainCamera;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Character");
        monologueText = GameObject.Find("SecondLevelStartText");
        dialogueSounds = player.GetComponents<AudioSource>();
    }


    void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player"))
        {
           StartCoroutine(gravitySwitch());
        }
    }
    IEnumerator gravitySwitch()
    {
        
        yield return new WaitForSeconds(0.5f);
        dialogueSounds[2].Play(0);
        monologueText.GetComponent<Text>().text = "Maybe someone from one these houses knows the way...";
        yield return new WaitForSeconds(3);
        flyBehaviour.ToggleFly();
        monologueText.GetComponent<Text>().text = "";
    }
        private void freezePlayer()
    {
        gameObject.GetComponent<Animator>().SetFloat("Speed", 0);
        gameObject.GetComponent<BasicBehaviour>().enabled = false;
        mainCamera.GetComponent<ThirdPersonOrbitCamBasic>().enabled = false;
    }
        private void unfreezePlayer()
    {
        gameObject.GetComponent<Animator>().SetFloat("Speed", 0.2f);
        gameObject.GetComponent<BasicBehaviour>().enabled = true;
        mainCamera.GetComponent<ThirdPersonOrbitCamBasic>().enabled = true;
    }
       
    
}