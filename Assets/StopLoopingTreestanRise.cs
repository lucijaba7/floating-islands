using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StopLoopingTreestanRise : MonoBehaviour
{
    private Animator treestanAnimator;
    private Animator characterAnimator;
    [SerializeField] GameObject character;
    private GameObject newSceneTransition;
    private Camera mainCamera;
    private Camera throwCamera;
    private bool newSceneFly = false;
    private void Start()
    {
        newSceneTransition = GameObject.Find("NewSceneTransition");
    }
    private void Update()
    {
        if(newSceneFly) character.transform.position = Vector3.MoveTowards(character.transform.position, newSceneTransition.transform.position, 0.4f);
    }
    void StopLoop()
    {
        treestanAnimator = GameObject.Find("TreestanLud").GetComponent<Animator>();
        treestanAnimator.SetBool("ActivateRise", false);
    }
    void flyAwayFromTree()
    {
        character.SetActive(true);
        newSceneFly = true;
        //mainCamera = GameObject.Find("Main Camera").GetComponent<Camera>();
        //throwCamera = GameObject.Find("ThrowCamera").GetComponent<Camera>();
        //throwCamera.enabled = false;
        //mainCamera.enabled = true;
        //unFreezePlayer(true);

    }
    void unFreezePlayer(bool unfreeze)
    {
        character = GameObject.Find("Character Phlo");
        character.GetComponent<BasicBehaviour>().enabled = unfreeze;
        character.GetComponent<FlyBehaviour>().enabled = unfreeze;
        character.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
        character.GetComponent<Animator>().SetBool("startedFlying", true);
    }
    void finishedRising()
    {
        treestanAnimator = GameObject.Find("TreestanLud").GetComponent<Animator>();
        treestanAnimator.SetBool("finishedRising", true);
    }
}