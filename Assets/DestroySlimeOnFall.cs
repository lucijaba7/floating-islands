using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroySlimeOnFall : MonoBehaviour
{
    private GameObject WorldBarriers;

    private void OnTriggerEnter(Collider other)
    {
        WorldBarriers = GameObject.Find("WorldBarriers");
        for (int i = 0; i < WorldBarriers.transform.childCount; i++)
        {
            if (other.gameObject.name == WorldBarriers.transform.GetChild(i).name)
            {
                Destroy(gameObject);
            }
        }
    }
}
