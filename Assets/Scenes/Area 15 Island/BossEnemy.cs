using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class BossEnemy : MonoBehaviour
{
    AudioSource[] shootingSounds; //Zvuk pucnja Nerra
    // reference to agent
    public NavMeshAgent agent;

    // reference to Nerr
    public GameObject nerr;

    // transform for the player
    public Transform player;

    // Nerr animator
    private Animator nerrAnimator;

    // for attack animation
    private float time = 0.0f;
    // // bullet time
    // private float bulletTime = 0.0f;

    // Bullet
    [SerializeField]
    GameObject bullet;

    // Bullet position
    [SerializeField]
    GameObject bulletPosition;

    // Health
    [SerializeField] int health;
    public NerrHealthBar healthBar;

    // die particle
    [SerializeField] GameObject dieParticle;
    
    // Mysterious object
    [SerializeField] GameObject mysteriousObject;

    // // Player movement
    // [SerializeField] ThrowBehaviour throwBehaviour;

    // Player damage
    [SerializeField] TakeDamage takeDamage;

    [SerializeField] BossFight bossFight;
    [SerializeField] Area15Gameplay area15Gameplay;

    // death
    private bool isDead = false;

    // for physical attack
    private bool isAttacking = false;


    private void Awake()
    {
        shootingSounds = GetComponents<AudioSource>(); //Zvuk pucnja Nerra
        player = GameObject.Find("Phlo").transform;
        agent = GetComponent<NavMeshAgent>();
        nerrAnimator = agent.GetComponent<Animator>();
        healthBar.SetMaxHealth(health);
    }

    private void Start()
    {
        InvokeRepeating("AttackPlayer", 5.0f, 15.0f);
        agent.speed = 0.005f;
    }


    void Update()
    {
        if(health > 0)
        {
            ChasePlayer();
            transform.LookAt(player);
        }
    }

    private void ChasePlayer()
    {
        agent.SetDestination(player.position);
    }

    private void AttackPlayer()
    {
        agent.SetDestination(transform.position);
        nerrAnimator.SetTrigger("attack");
        transform.LookAt(player);
        //4:08
        StartCoroutine(ShootPhlo());

    }

    IEnumerator ShootPhlo()
    {
        yield return new WaitForSeconds(1);
        float fireRate = 0.1f;
        do
        {
            shootingSounds[0].Play(0);
            Rigidbody rb = Instantiate(bullet, bulletPosition.transform.position, Quaternion.identity).GetComponent<Rigidbody>();
            rb.transform.LookAt(player);
            rb.transform.Rotate(new Vector3(0, 90, 0));
            Vector3 temp = (player.position - transform.position);
            Vector3 positionToShoot = new Vector3(temp.x, -3f, temp.z);
            rb.AddForce(positionToShoot.normalized * 30f, ForceMode.Impulse);
            // rb.AddForce(agent.transform.up * 0.5f , ForceMode.Impulse);

            time += Time.deltaTime + fireRate;
            yield return new WaitForSeconds(fireRate);
        } while (time < 2.5f && !isDead);

        time = 0.0f;

    }
   
    public void resetNerrHealth()
    {
        healthBar.SetMaxHealth(health);
    }
    
    void OnCollisionEnter(Collision co)
    {
        if(co.collider.tag == "phloBullet")
        {
            Debug.Log("Phlo lupio");
            health -=1;
            healthBar.SetHealth(health);
            if(health < 0) StartCoroutine(Die());
        }
  
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player") && !isAttacking)
        {
            StartCoroutine(PhysicalAttack());
        } 

    }

    private IEnumerator PhysicalAttack()
    {
        isAttacking = true;
        nerrAnimator.SetTrigger("physicalAttack");
        // Rigidbody playerRb = player.parent.GetComponent<Rigidbody>();
        yield return new WaitForSeconds(0.4f);
        takeDamage.physicalAttackDamage();
        yield return new WaitForSeconds(4);
        isAttacking = false;
        // playerRb.AddForce((player.position - transform.position), 200f , ForceMode.Impulse);
        yield return null;
    }

    private IEnumerator Die()
    {   
        isDead = true;
        nerrAnimator.SetTrigger("die");
        yield return new WaitForSeconds(2);
        Instantiate(dieParticle, nerr.transform.position, Quaternion.identity);
        Instantiate(mysteriousObject, nerr.transform.position, Quaternion.identity);
        bossFight.EndBossFight();
        nerr.SetActive(false);
        CancelInvoke("AttackPlayer");
        area15Gameplay.TurnOffBgMusic();
        // nerr.GetComponent<UnityEngine.AI.NavMeshAgent>().enabled = false;
        // nerr.transform.position = new Vector3(nerr.transform.position.x, -90f, nerr.transform.position.z);
    }


}
