using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class StartArea15Island : MonoBehaviour
{
    GameObject newSceneTransition;
    [SerializeField] GameObject startScreen;
    [SerializeField] Slider loading;
    void Start()
    {
        newSceneTransition = GameObject.Find("NewSceneTransition");
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == newSceneTransition.name) StartCoroutine(startArea15Island());
    }

    private IEnumerator startArea15Island()
    {
        startScreen.SetActive(true);
        yield return new WaitForSeconds(2f);
        loading.value = 0.2f;
        yield return new WaitForSeconds(2f);
        loading.value = 0.6f;
        yield return new WaitForSeconds(2f);
        loading.value = 1f;
        SceneManager.LoadScene("Area 15 Island");
        yield return new WaitForSeconds(0.1f);
        startScreen.SetActive(false);
    }
}