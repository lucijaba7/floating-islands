
using UnityEngine;
using TMPro;


public class PhloShoot : MonoBehaviour
{
    //bullet 
    public GameObject bullet;

    //Phlo
    [SerializeField] GameObject player;
    private Animator phloAnimator;

    //bullet force
    public float shootForce;

    //Gun stats
    public int magazineSize;
    public bool allowButtonHold;
    AudioSource[] shootingSound;

    int bulletsLeft;


    //bools
    bool shooting;

    //Reference
    public Camera fpsCam;
    public Transform attackPoint;

    //Slider
    [SerializeField] PhloPower phloPower;


    //bug fixing :D
    public bool allowInvoke = true;

    private void Awake()
    {
        bulletsLeft = 0;
        phloAnimator = player.GetComponent<Animator>();
        shootingSound = GetComponents<AudioSource>();
    }

    private void Update()
    {
        MyInput();
    }

    private void MyInput()
    {
        //Check if allowed to hold down button and take corresponding input
        if (allowButtonHold) shooting = Input.GetKey(KeyCode.Mouse0);
        else shooting = Input.GetKeyDown(KeyCode.Mouse0);

        if(shooting && bulletsLeft > 0) Shoot();
        else {
            phloAnimator.SetBool("shootingNerr", false);
        }
    }

    private void Shoot()
    {
        shootingSound[0].Play(0);
        phloAnimator.SetBool("shootingNerr", true);
        // readyToShoot = false;

        //Find the exact hit position using a raycast
        Ray ray = fpsCam.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0)); //Just a ray through the middle of your current view

        //Get target point
        Vector3 targetPoint;
        targetPoint = ray.GetPoint(75); //Just a point far away from the player

        //Calculate direction from attackPoint to targetPoint
        Vector3 directionToTarget = targetPoint - attackPoint.position;

        //Instantiate bullet/projectile
        GameObject currentBullet = Instantiate(bullet, attackPoint.position, Quaternion.identity); //store instantiated bullet in currentBullet
        
        //Rotate bullet to shoot direction
        // currentBullet.transform.forward = directionWithSpread.normalized;
  
        //Add forces to bullet
        currentBullet.GetComponent<Rigidbody>().AddForce(directionToTarget.normalized * shootForce, ForceMode.Impulse);

        bulletsLeft -= 7;
        phloPower.SetPower(bulletsLeft);
    }

    public void fillPowerMagazine()
    {
        bulletsLeft = magazineSize;
        phloPower.FillMagazine(magazineSize);
    }

    public void resetPowerToZero()
    {
        bulletsLeft = 0;
        phloPower.SetPower(0);
    }
}
