using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateSphere : MonoBehaviour
{

    private GameObject phloSphere;
    private Animator characterAnimator;
    void Update()
    {
        phloSphere = GameObject.Find("2 Phlo-Sfera");
        characterAnimator = GameObject.Find("Character Phlo Sfera").GetComponent<Animator>();
        if (phloSphere != null && characterAnimator.GetBool("Grounded"))
            if (Input.GetButton("Vertical") || Input.GetButton("Horizontal"))
                phloSphere.transform.Rotate(Vector3.right * (Time.deltaTime * 0.5f) * 360);
    }
}