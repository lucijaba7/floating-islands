using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ChangeSceneRadioactive : MonoBehaviour
{
    GameObject newSceneTransition;
    void Start()
    {
        newSceneTransition = GameObject.Find("NewSceneTransition");
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == newSceneTransition.name) SceneManager.LoadScene("Area 15");
    }
}
