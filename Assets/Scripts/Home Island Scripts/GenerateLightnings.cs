using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenerateLightnings : MonoBehaviour
{
    private int numOfRanLightnings = 5;
    public float MaxXLength = (float)60.0;
    public float MinXLength = (float)-175.0;
    public float MaxZLength = (float)6.5;
    public float MinZLength = (float)-4.4;


    public GameObject LightningBolt;
    public GameObject LightningBolts;
    private void Spawn()
    {
        float randomX = Random.Range(MinXLength, MaxXLength);
        float randomZ = Random.Range(MinZLength, MaxZLength);
        GameObject instantiatedObject = Instantiate(LightningBolt, new Vector3(randomX, (float)0, randomZ), transform.rotation = Quaternion.Euler(0, 0, 0));
        instantiatedObject.AddComponent<DissapearLightning>();
        instantiatedObject.transform.SetParent(LightningBolts.transform, worldPositionStays: false);
        instantiatedObject.transform.position = new Vector3(randomX, 30, randomZ);
        instantiatedObject.SetActive(true);
    }
    void Start()
    {
        LightningBolt = GameObject.Find("LightningBolt");
        LightningBolts = GameObject.Find("Lightning Bolts");
    }
    private void Update()
    {
        if(LightningBolts.transform.childCount < numOfRanLightnings) Spawn();
    }
}
