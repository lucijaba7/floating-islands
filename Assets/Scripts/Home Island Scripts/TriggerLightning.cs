using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerLightning : MonoBehaviour
{
    private Vector3 originalPosition;
    private void Start()
    {
        originalPosition = transform.localPosition;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "LightningBolt")
        {
            gameObject.GetComponent<CharacterController>().enabled = false;
            gameObject.transform.localPosition = originalPosition;
            gameObject.GetComponent<CharacterController>().enabled = true;
        }
    }
}
