using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StepsWalkScript : MonoBehaviour
{
    [SerializeField] GameObject stepRayUpper;
    [SerializeField] GameObject stepRayLower;
    [SerializeField] float stepHeight;
    [SerializeField] float stepSmooth;
    public Rigidbody rigidBody; 
    // Start is called before the first frame update
    private void Awake()
    {
        stepRayUpper.transform.position = new Vector3(stepRayUpper.transform.position.x, stepHeight, stepRayUpper.transform.position.z);
        rigidBody = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        stepClimb();
    }
    void stepClimb()
    {  
        RaycastHit hitLower;
        if(Physics.Raycast(stepRayLower.transform.position, transform.TransformDirection(Vector3.forward), out hitLower,0.3f))
        {
            RaycastHit hitUpper;
                if(Physics.Raycast(stepRayUpper.transform.position, transform.TransformDirection(Vector3.forward),out hitUpper, 0.5f))
                {
                    rigidBody.position -= new Vector3(0f, -stepSmooth, 0f);
                }
        }
        RaycastHit hitLower45;
        if(Physics.Raycast(stepRayLower.transform.position, transform.TransformDirection(1.5f, 0, 1), out hitLower45,0.3f))
        {
            RaycastHit hitUpper45;
                if(Physics.Raycast(stepRayUpper.transform.position, transform.TransformDirection(1.5f, 0, 1),out hitUpper45, 0.5f))
                {
                    rigidBody.position -= new Vector3(0f, -stepSmooth, 0f);
                }
        }
RaycastHit hitLowerMinus45;
        if(Physics.Raycast(stepRayLower.transform.position, transform.TransformDirection(-1.5f, 0, 1), out hitLowerMinus45,0.3f))
        {
            RaycastHit hitUpperMinus45;
                if(Physics.Raycast(stepRayUpper.transform.position, transform.TransformDirection(-1.5f, 0, 1),out hitUpperMinus45, 0.5f))
                {
                    rigidBody.position -= new Vector3(0f, -stepSmooth, 0f);
                }
        }
    }
}
