using UnityEngine;

public class LightningBoltsSpawnScript : MonoBehaviour
{
    public float MaxXLength = (float)60.0;
    public float MinXLength = (float)-175.0;
    public float MaxZLength = (float)6.5;
    public float MinZLength = (float)-4.4;

    public bool spawn = false;
    public float timer = 0.0f;
    public int waitTimeSpawn = 5;
    public int waitTimeDissapear = 2;

    public GameObject LightningBolt;

    private void Spawn()
    {
        float randomX = Random.Range(MinXLength, MaxXLength);
        float randomZ = Random.Range(MinZLength, MaxZLength);

        LightningBolt.transform.position = new Vector3(randomX, (float)1.15, randomZ);
        LightningBolt.SetActive(true);
    }
    private void Dissapear(GameObject gameObject)
    {
        LightningBolt.SetActive(false);
    }

    private void Start()
    {
        LightningBolt = GameObject.Find("LightningBolt");
    }
    void Update()
    {
        timer += Time.deltaTime;
        if (timer > waitTimeDissapear)
        {
            if (LightningBolt.activeInHierarchy == true) Dissapear(LightningBolt);
            if (timer > waitTimeSpawn)
            {
                timer = 0.0f;
                spawn = true;
            }
        }
        else 
            if (spawn) {
                Spawn();
                spawn = false;
            }
    }
}
