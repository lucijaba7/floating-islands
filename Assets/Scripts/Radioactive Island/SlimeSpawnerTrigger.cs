using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlimeSpawnerTrigger : MonoBehaviour
{

    private GameObject Slimes;
    public GameObject Slime;   
    public float MaxWidth = (float)33;
    public float MinWidth = (float)8.5;
    public float MaxLength = (float)-3.8;
    public float MinLength = (float)-12;
    
    private void OnTriggerEnter(Collider other)
    {
        float randomX = Random.Range(MinWidth, MaxWidth);
        float randomZ = Random.Range(MinLength, MaxLength);
        
        //collider sa Slime - ako se dogodi collide obriši Slime i smanji brojač za 1
        if (other.gameObject.name == "Slime(Clone)")
            {
                Debug.Log("Collide!!");
                //Destroy(other);
                Instantiate(Slime, new Vector3(randomX, (float)3.6233, randomZ), Quaternion.Euler(new Vector3(0, Random.Range(0, 360), 0)));
                /*
                newSlime.transform.parent = Slimes.transform;
                if(newSlime.activeSelf){
                    Debug.Log("AKtivan!");
                }
                */
            } 
        }
    
    void Start()
    {
        Slime = GameObject.Find("Slime");
        Slimes = GameObject.Find("Slimes");
    }
}
