// Skripta za detektiranje kolizije Slimeova sa barijerama i brojač Slimeova
// 
// 

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SlimeTrigger : MonoBehaviour
{
    //Početna vrijednost brojača
    int SlimesCounter = 0;
    private GameObject SlimeCounterText;
    bool proceed = false;
    bool isDestroyed = false;

    private void OnTriggerEnter(Collider other)
    {
        proceed = false;
        

        if(other.CompareTag("Player")) proceed = true;

        if (proceed && !isDestroyed)
        {
            isDestroyed = true;
            gameObject.GetComponent<AudioSource>().Play(0);
            Destroy(gameObject.GetComponent<BoxCollider>());
            Destroy(gameObject.GetComponent<SphereCollider>());
            //other.gameObject.SetActive(false);
            SlimeCounterText = GameObject.Find("SlimeCounterText");
            SlimesCounter = int.Parse(SlimeCounterText.GetComponent<Text>().text.Substring(18));
            SlimesCounter--;
            //Prikaz brojača
            SlimeCounterText.GetComponent<Text>().text = "Slimes remaining: " + SlimesCounter;
        } 
    }
    private void Update()
    {
        if (gameObject.transform.position.y < -250) Destroy(gameObject);
    }
}
