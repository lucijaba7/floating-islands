using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SlimeSpawner : MonoBehaviour{
    AudioSource[] introSounds;
    private GameObject Slimes;
    private const int numOfRanSlimes = 300;    
    public float MaxWidth = (float)33;
    public float MinWidth = (float)8.5;
    public float MaxLength = (float)-3.8;
    public float MinLength = (float)-12;
    
    public GameObject Slime;

    void Spawn(){
     
       float randomX = Random.Range(MinWidth, MaxWidth);
       float randomZ = Random.Range(MinLength, MaxLength);

       var newSlime = Instantiate(Slime, new Vector3(randomX, (float)3.6233, randomZ), Quaternion.Euler(new Vector3(0, Random.Range(0, 360), 0)));
       newSlime.transform.parent = Slimes.transform;
       }

     void SpawnPerPhase()
     {
         for (int i = 0; i < numOfRanSlimes; i++)
         {
             Spawn();
         }
     }
    void Start()
    {
        Slime = GameObject.Find("Slime");
        Slimes = GameObject.Find("Slimes");
        SpawnPerPhase();
    }
    private void Update()
    {
        // text = SlimeCounterText.GetComponent<Text>().text;
        // slimeCounter = int.Parse(text.Substring(18));
        // if ((slimeCounter < 210 && slimeCounter > 200 && firstTimeSpawningFirstPhase) || (slimeCounter < 110 && slimeCounter > 100 && firstTimeSpawningSecondPhase))
        //     SpawnPerPhase();
    }
}