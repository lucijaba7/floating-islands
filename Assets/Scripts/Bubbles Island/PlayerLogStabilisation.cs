using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerLogStabilisation : MonoBehaviour
{
    private GameObject FloatingTreeLogs;
    private GameObject CharacterAndMovement;
    private GameObject PlayabilityElements;
    void Start()
    {
        FloatingTreeLogs = GameObject.Find("FloatingTreeLogs");
        CharacterAndMovement = GameObject.Find("Character and Movement");
        PlayabilityElements = GameObject.Find("Playability Elements");
    }

    void OnTriggerEnter(Collider other)
    {
        for(int i = 0; i < FloatingTreeLogs.transform.childCount; i++)
        {
            if (other.gameObject.name == FloatingTreeLogs.transform.GetChild(i).name)
                CharacterAndMovement.transform.parent = other.gameObject.transform;
        }
    }
    void OnTriggerExit(Collider other)
    {
        CharacterAndMovement.transform.parent = PlayabilityElements.transform;
    }
}
