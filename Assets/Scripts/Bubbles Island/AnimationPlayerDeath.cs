using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationPlayerDeath : MonoBehaviour
{
    private GameObject FloatingTreeLogs;
    private GameObject CharacterAndMovement;
    private GameObject BubblesIsland;
    void Start()
    {
        FloatingTreeLogs = GameObject.Find("FloatingTreeLogs");
        CharacterAndMovement = GameObject.Find("Character and Movement");
        BubblesIsland = GameObject.Find("Bubbles Island");

        CharacterAndMovement.transform.parent = BubblesIsland.transform;
    }
}
