using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TriggerFallDeath : MonoBehaviour
{
    private GameObject WorldBarriers;
    private void Start()
    {
        WorldBarriers = GameObject.Find("WorldBarriers");
    }
    private void OnTriggerEnter(Collider other)
    {
        for(int i = 0; i < WorldBarriers.transform.childCount; i++)
        {
            if (other.gameObject.name == WorldBarriers.transform.GetChild(i).name)
            {
                Scene scene = SceneManager.GetActiveScene(); 
                SceneManager.LoadScene(scene.name);
            }
        }
    }
}
