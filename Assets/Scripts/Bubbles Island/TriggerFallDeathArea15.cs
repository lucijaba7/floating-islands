using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TriggerFallDeathArea15 : MonoBehaviour
{
    private GameObject WorldBarriers;
    [SerializeField] GameObject healthCanvas;
    [SerializeField] BossFight bossFight;

    private void Start()
    {
        WorldBarriers = GameObject.Find("WorldBarriers");
    }
    private void OnTriggerEnter(Collider other)
    {
        for(int i = 0; i < WorldBarriers.transform.childCount; i++)
        {
            if (other.gameObject.name == WorldBarriers.transform.GetChild(i).name)
            {
                Debug.Log("NERR ACTIVE");
                if(healthCanvas.active)
                {
                    bossFight.ResetBossFight();
                }
                else
                {
                    Scene scene = SceneManager.GetActiveScene(); 
                    SceneManager.LoadScene(scene.name);
                }
            }
        }
    }
}
