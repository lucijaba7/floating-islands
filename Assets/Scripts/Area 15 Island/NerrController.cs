using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NerrController : MonoBehaviour
{
	private int groundedBool;  
    private Animator anim;    
    private Vector3 colExtents;  
    //Ground Check
    [SerializeField] private Transform groundCheck;
    [SerializeField] private float groundRadius;
    [SerializeField] private LayerMask whatIsGround; 
    [SerializeField] public GameObject explotion;

    // Start is called before the first frame update
    void Awake()
    {
        anim = GetComponent<Animator> ();
		groundedBool = Animator.StringToHash("Grounded");
        colExtents = GetComponent<Collider>().bounds.extents;
        
    }

    // Update is called once per frame
    void Update()
    {
        bool isGrounded = IsGrounded();
        anim.SetBool(groundedBool, isGrounded);
        if (isGrounded && anim.GetBool("NerrFalling")){
            this.GetComponent<NavMeshAgent>().enabled = true;

        }
    }
    
    // Function to tell whether or not the player is on ground.
	public bool IsGrounded()
	{
        // return Physics.CheckSphere(groundCheck.position, groundRadius, whatIsGround);
		Ray ray = new Ray(this.transform.position + Vector3.up * 2 * colExtents.x, Vector3.down);
		return Physics.SphereCast(ray, colExtents.x, colExtents.x + 0.2f);
	}

    private void OnLand(AnimationEvent animationEvent)
	{
        Instantiate(explotion, this.transform.position, Quaternion.identity);
		// AudioSource.PlayClipAtPoint(LandingAudioClip, this.transform.position, FootstepAudioVolume);
	}

}
