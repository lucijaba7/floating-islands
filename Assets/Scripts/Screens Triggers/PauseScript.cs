using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseScript : MonoBehaviour
{
    private GameObject Screens;
    private GameObject PlayabilityElements;

    private void Awake()
    {
        Screens = GameObject.Find("Screens");
        PlayabilityElements = GameObject.Find("Playability Elements");
    }
    private void FixedUpdate()
    {
        if (Input.GetKey(KeyCode.P))
        {
            Debug.Log("P stisnut");
            Time.timeScale = 0;
            activateCurrentChildsOfBubblesIsland(false);
            Screens.transform.GetChild(1).gameObject.SetActive(true);
        }
    }
    private void activateCurrentChildsOfBubblesIsland(bool boolean)
    {
        for (int i = 0; i < PlayabilityElements.transform.childCount; i++)
        {
            if(GameObject.Find("Playability Elements")!=null){
                PlayabilityElements.transform.GetChild(i).gameObject.SetActive(boolean);
            }
        }
    }
}
