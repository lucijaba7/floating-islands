using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayExitOnClickTrigger : MonoBehaviour
{
	private Button button;
	private GameObject PlayabilityElements;
	private GameObject StartScreen;
	private GameObject MonologuesAndHelpBox;

    private void Awake()
    {
		StartScreen = GameObject.Find("Start Screen");
		PlayabilityElements = GameObject.Find("Playability Elements");
		MonologuesAndHelpBox = GameObject.Find("Monologues and HelpBox");
	}

	private void Start()
	{
		activateCurrentChildsOfBubblesIsland(false);

		button = GetComponent<Button>();
		button.onClick.AddListener(TaskOnClick);
	}

	private void TaskOnClick()
	{
		if (button.name == "PLAY button")
		{
			Debug.Log("Gumb stisnut");
			StartScreen.SetActive(false);
			activateCurrentChildsOfBubblesIsland(true);
		}
		else Application.Quit();
	}

	private void activateCurrentChildsOfBubblesIsland(bool boolean)
	{
		for (int i = 0; i < PlayabilityElements.transform.childCount; i++)
		{
			PlayabilityElements.transform.GetChild(i).gameObject.SetActive(boolean);
		}
	}
}
