using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class OnPauseScript : MonoBehaviour
{
    private GameObject Screens;
    private GameObject PlayabilityElements;

    private void Awake()
    {
        Screens = GameObject.Find("Screens");
        PlayabilityElements = GameObject.Find("Playability Elements");
    }
    private void Update()
    {
        if (Input.GetKeyDown("space"))
        {
            Time.timeScale = 1;
            Screens.transform.GetChild(1).gameObject.SetActive(false);
            activateCurrentChildsOfBubblesIsland(true);
        }
        else if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
    }
    private void activateCurrentChildsOfBubblesIsland(bool boolean)
    {
        for(int i = 0; i < PlayabilityElements.transform.childCount; i++)
        {
            PlayabilityElements.transform.GetChild(i).gameObject.SetActive(boolean);
        }
    }
}
