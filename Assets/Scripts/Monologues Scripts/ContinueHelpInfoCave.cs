using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ContinueHelpInfoCave : MonoBehaviour
{
    private GameObject MonologuesAndHelpBox;
    private GameObject HelpInfo;
    private bool isActiveJumpText = false;

    void Awake()
    {
        MonologuesAndHelpBox = GameObject.Find("Monologues and HelpBox");
        HelpInfo = GameObject.Find("Help Info");
    }
    private void Update()
    {
        if (Input.GetKeyDown("space"))
            deActivateHelpBox();
    }
    private void deActivateHelpBox()
    {
        MonologuesAndHelpBox.transform.GetChild(1).gameObject.SetActive(false);
    }
}
