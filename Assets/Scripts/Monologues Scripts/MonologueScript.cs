using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class MonologueScript : MonoBehaviour
{
    AudioSource[] dialogueSounds;

    private GameObject MonologuesAndHelpBox;
    private GameObject Screens;

    public GameObject textBox;
    private void Awake()
    {
        MonologuesAndHelpBox = GameObject.Find("Monologues and HelpBox");
        textBox = GameObject.Find("AfterCrashText");
        Screens = GameObject.Find("Screens");
    }

    private void Start()
    {
        dialogueSounds = GetComponents<AudioSource>();
        if(!Screens.transform.GetChild(0).gameObject.activeSelf || !Screens.transform.GetChild(1).gameObject.activeSelf)
            StartCoroutine(showSubtitles());
    }
    IEnumerator showSubtitles()
    {
        yield return new WaitForSeconds(8);
        textBox.GetComponent<Text>().text = "Phlo: Oh no. Well, I guess I have to keep going";
        dialogueSounds[0].Play(0);
        yield return new WaitForSeconds(8);
        textBox.GetComponent<Text>().text = null;
        activateHelpBox();
    }
    private void activateHelpBox()
    {
        MonologuesAndHelpBox.transform.GetChild(1).gameObject.SetActive(true);
    }
}
