using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ContinueHelpInfo : MonoBehaviour
{
    private GameObject MonologuesAndHelpBox;
    private GameObject HelpInfo;
    private GameObject Title;
    private bool isActiveJumpText = false;

    void Awake()
    {
        MonologuesAndHelpBox = GameObject.Find("Monologues and HelpBox");
        HelpInfo = GameObject.Find("Help Info");
        Title = GameObject.Find("Title");
    }
    void Start()
    {
        StartCoroutine(showSubtitles());
    }
    IEnumerator showSubtitles()
    {   
        MonologuesAndHelpBox.transform.GetChild(1).gameObject.SetActive(true);
        yield return new WaitForSeconds(3);
        Title.GetComponent<Text>().text = "Movement";
        HelpInfo.GetComponent<Text>().text = "You can jump using SPACE on your keyboard.";
        yield return new WaitForSeconds(3);
        Title.GetComponent<Text>().text = "General";
        HelpInfo.GetComponent<Text>().text = "Pause the game at any time using the P button.";
        yield return new WaitForSeconds(3);
        deActivateHelpBox();
    }
    private void deActivateHelpBox()
    {
        MonologuesAndHelpBox.transform.GetChild(1).gameObject.SetActive(false);
    }
}
