using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DoorbellTriggerChaseStart : MonoBehaviour
{
    private GameObject player;
    [SerializeField] GameObject giant;
    [SerializeField] Text interactionText;
    private GameObject doorbell;
    private bool inZone = false;
    [SerializeField] Camera mainCamera;
    [SerializeField] AudioSource knockingSound;

    // Start is called before the first frame update
    void Start()

    {
        player = GameObject.Find("Character");
        doorbell = GameObject.Find("DoorbellTrigger");
        
    }
     void OnTriggerEnter(Collider other)
    {
       if (other.gameObject.name == doorbell.name && interactionText.GetComponent<Text>().text == "") inZone = true;
    }


    // Update is called once per frame
    void Update()
    {
        if(inZone)
        {
            interactionText.GetComponent<Text>().text = "Press F to knock on the door!";
            if(Input.GetButtonDown("Fire1"))
            {
                freezePlayer();
                StartCoroutine(RingDoorbell());

            }
            
            

            
            



        }
        
    }
    IEnumerator RingDoorbell()
    {   
        player.GetComponent<Animator>().Play("Button Pushing");
        knockingSound.Play(0);
        yield return new WaitForSeconds(2);
        giant.SetActive(true);
        unfreezePlayer();
        inZone = false;
        interactionText.GetComponent<Text>().text = "";

            

    }
private void freezePlayer()
    {
        gameObject.GetComponent<Animator>().SetFloat("Speed", 0);
        gameObject.GetComponent<BasicBehaviour>().enabled = false;
        mainCamera.GetComponent<ThirdPersonOrbitCamBasic>().enabled = false;
    }
        private void unfreezePlayer()
    {
        gameObject.GetComponent<Animator>().SetFloat("Speed", 0.2f);
        gameObject.GetComponent<BasicBehaviour>().enabled = true;
        mainCamera.GetComponent<ThirdPersonOrbitCamBasic>().enabled = true;
    }
}

