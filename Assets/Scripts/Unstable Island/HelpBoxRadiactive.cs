using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HelpBoxRadiactive : MonoBehaviour
{
    private GameObject MonologuesAndHelpBox;
    private GameObject HelpInfo;
    private GameObject Title;
    private bool isActiveJumpText = false;

    void Awake()
    {
        MonologuesAndHelpBox = GameObject.Find("Monologues and HelpBox");
        HelpInfo = GameObject.Find("Help Info");
        Title = GameObject.Find("Title");
    }
    void Start()
    {
        StartCoroutine(showSubtitles());
    }
    IEnumerator showSubtitles()
    {   yield return new WaitForSeconds(4);
        MonologuesAndHelpBox.transform.GetChild(0).gameObject.SetActive(true);
        yield return new WaitForSeconds(3);
        Title.GetComponent<Text>().text = "Combat";
        HelpInfo.GetComponent<Text>().text = "Use Left Mouse Button and Right Mouse Button...";
        yield return new WaitForSeconds(2);
        HelpInfo.GetComponent<Text>().text = "to attack with left and right hands respectively.";
        yield return new WaitForSeconds(3);
        deActivateHelpBox();
    }
    private void deActivateHelpBox()
    {
        MonologuesAndHelpBox.transform.GetChild(0).gameObject.SetActive(false);
    }
}
