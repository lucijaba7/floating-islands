using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BoardTrigger : MonoBehaviour
{
    private GameObject board;
    private bool inZone = false;
    private GameObject boardMonologue;
    private GameObject character;
    AudioSource[] monologueSounds;
    private bool hasPlayed = false;
    Animator playerAnimator;
    [SerializeField] FlyBehaviour flyBehaviour;
    [SerializeField] Camera mainCamera;

    // Start is called before the first frame update
    void Start()
    {
        monologueSounds = GetComponents<AudioSource>();
        board = GameObject.Find("Board");
        character = GameObject.Find("Character");
        boardMonologue = GameObject.Find("BoardMonologue");
        playerAnimator = character.GetComponent<Animator>();



        
        
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == board.name) inZone = true;
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.name == board.name) inZone = false;
    }

    // Update is called once per frame
    void Update()
    {
        if(inZone == true && boardMonologue.GetComponent<Text>().text == "" && hasPlayed==false)
        {
           StartCoroutine(startMonologues());
           hasPlayed = true;
           

            
           

           
           



        }

    }
    IEnumerator startMonologues()
    {
        freezePlayer();
        yield return new WaitForSeconds(2);
        monologueSounds[1].Play(0);
        boardMonologue.GetComponent<Text>().text = "So that's what makes the island unstable. The satellite makes the core of the island so unstable, the gravitational field weakens.";
        yield return new WaitForSeconds(8);
        boardMonologue.GetComponent<Text>().text = "Maybe I can use that to my advantage.";
        yield return new WaitForSeconds(3);
        boardMonologue.GetComponent<Text>().text = null;
        new WaitForSeconds(5);
        unfreezePlayer();
        flyBehaviour.ToggleFly();


}
    private void freezePlayer()
    {
        gameObject.GetComponent<Animator>().SetFloat("Speed", 0);
        gameObject.GetComponent<BasicBehaviour>().enabled = false;
        mainCamera.GetComponent<ThirdPersonOrbitCamBasic>().enabled = false;
    }
    private void unfreezePlayer()
    {
        gameObject.GetComponent<Animator>().SetFloat("Speed", 0.2f);
        gameObject.GetComponent<BasicBehaviour>().enabled = true;
        mainCamera.GetComponent<ThirdPersonOrbitCamBasic>().enabled = true;
    }
}
