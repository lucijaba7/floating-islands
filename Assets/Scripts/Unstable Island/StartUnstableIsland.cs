using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StartUnstableIsland : MonoBehaviour
{
    [SerializeField] GameObject startScreen;
    [SerializeField] Slider loading;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(startUnstableIsland());
    }

    private IEnumerator startUnstableIsland()
    {
        startScreen.SetActive(true);
        yield return new WaitForSeconds(0.3f);
        loading.value = 0.2f;
        yield return new WaitForSeconds(0.3f);
        loading.value = 0.6f;
        yield return new WaitForSeconds(0.3f);
        loading.value = 1f;
        yield return new WaitForSeconds(0.3f);
        startScreen.SetActive(false);
    }

}
