using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageAndDeathScript : MonoBehaviour
{
 [SerializeField] Vector3 startPosPlayer;
 public Vector3 startPosGiant;
 [SerializeField] Animator golemAni;
 [SerializeField] Animator playerAni;
private bool enableAct;

 [SerializeField] GameObject attackColliderObject;

 private int PhloHealth = 10;
 private bool inDamageRange = false;
 [SerializeField] GameObject giant;
 [SerializeField]GameObject player;
 [SerializeField] Collider attackCollider;
 void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == attackColliderObject.name){
            inDamageRange = true;
        }

    }




 private void start()
 {
     player = GameObject.Find("Character");
     giant = GameObject.Find("GiantDummy");
     startPosGiant = giant.transform.position;
     golemAni = giant.GetComponent<Animator>();
     playerAni = GetComponent<Animator>();
     attackCollider = attackColliderObject.GetComponent<Collider>();
     
 }
 private void Update()
 { 
    /*if(inDamageRange){
        attackCollider.enabled = true;
        Debug.Log("in range");
    }*/
    if(golemAni.GetCurrentAnimatorStateInfo(0).IsName("Giant@UnarmedAttack01") && PhloHealth!=0 && inDamageRange&& playerAni.GetCurrentAnimatorStateInfo(1).IsName("No Flying")){
    
        PhloHealth = 0;
    }

    if(PhloHealth == 0)
        StartCoroutine(DeathAndRespawn());
        PhloHealth = 10;
 }
        
         

IEnumerator DeathAndRespawn(){
    yield return new WaitForSeconds(0.5f);
    playerAni.SetBool("Death",true);
    yield return new WaitForSeconds(3);
    golemAni.Play("Idle");
    giant.gameObject.SetActive(false);
    giant.transform.position = startPosGiant; 
    player.transform.position = startPosPlayer;
    playerAni.Play("Fall");
    playerAni.SetBool("Death",false);
    yield return new WaitForSeconds(5);
    giant.gameObject.SetActive(true);
}

}