using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StartTrigger : MonoBehaviour
{
    private GameObject startZone;
    private bool inZone = false;
    private GameObject startMonologue;
    private GameObject character;
    AudioSource[] monologueSounds;
    private bool hasPlayed = false;
    [SerializeField] GameObject board;
    [SerializeField] Camera mainCamera;

    // Start is called before the first frame update
    void Start()
    {
        monologueSounds = GetComponents<AudioSource>();
        startZone = GameObject.Find("StartZone");
        character = GameObject.Find("Character");
        startMonologue = GameObject.Find("StartText");


        
        
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == startZone.name) inZone = true;
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.name == startZone.name) inZone = false;
    }

    // Update is called once per frame
    void Update()
    {
        if(inZone == true && startMonologue.GetComponent<Text>().text == "" && hasPlayed==false)
        {
            StartCoroutine(startMonologues());
            hasPlayed = true;
            
           
           
            

        }
    }
     IEnumerator startMonologues()
    {
      
        freezePlayer();
        yield return new WaitForSeconds(2);
        monologueSounds[0].Play(0);
        startMonologue.GetComponent<Text>().text = "Something feels very off about this island. I have to find out what is happening.";
        yield return new WaitForSeconds(8);
        startMonologue.GetComponent<Text>().text = "Why do I feel so small? What's that noise?";
        yield return new WaitForSeconds(5);
        startMonologue.GetComponent<Text>().text = null;
        unfreezePlayer();

}
    private void freezePlayer()
    {
        gameObject.GetComponent<Animator>().SetFloat("Speed", 0);
        gameObject.GetComponent<BasicBehaviour>().enabled = false;
        mainCamera.GetComponent<ThirdPersonOrbitCamBasic>().enabled = false;
    }
        private void unfreezePlayer()
    {
        gameObject.GetComponent<Animator>().SetFloat("Speed", 0.2f);
        gameObject.GetComponent<BasicBehaviour>().enabled = true;
        mainCamera.GetComponent<ThirdPersonOrbitCamBasic>().enabled = true;
    }
}
