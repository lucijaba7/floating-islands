/*using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class LowerLevelTrigger : MonoBehaviour
{
    private GameObject startTrigger;
    private GameObject button;
    private GameObject wilburrGiant;
    private Animator wilburrAnimator;
    private bool inZone = false;
    private bool initiatedMonologue = false;
    public GameObject finalText;
    private GameObject helpText;
    private GameObject finalDialogue;
    private GameObject character;
    private Camera mainCamera;
    private Camera dialogueCamera;
    private Vector3 position;
    AudioSource[] dialogueSounds;
    void Start()
    {
        dialogueSounds = GetComponents<AudioSource>();

        finalZone = GameObject.Find("FinalZone");
        wilburrGiant = GameObject.Find("GiantWilburr");
        wilburrAnimator = wilburrGiant.GetComponent<Animator>();
        button = GameObject.Find("Button");
        helpText = GameObject.Find("HelpText");
        finalText = GameObject.Find("FinalText");
        finalDialogue = GameObject.Find("FinalDialogue");
        character = GameObject.Find("Character");
        mainCamera = Camera.main;
        dialogueCamera = GameObject.Find("DialogueCamera").GetComponent<Camera>();
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == finalZone.name) inZone = true;
    }
    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.name == finalZone.name) inZone = false;
    }
    private void Update()
    {
        if (Input.GetButtonDown("Fire1") && inZone
            && !initiatedMonologue
            && finalText.GetComponent<Text>().text == "")
        {

            initiatedMonologue = true;
            wilburrAnimator.SetTrigger("active");
            StartCoroutine(finalDialogues());
        }

        if (inZone && !initiatedMonologue && finalText.GetComponent<Text>().text == "") { helpText.GetComponent<Text>().text = "Press 'E' to open"; }
        else helpText.GetComponent<Text>().text = null;
    }

    bool isPlaying(Animator anim, string stateName)
    {
        int animLayer = 0;

        if (anim.GetCurrentAnimatorStateInfo(animLayer).IsName(stateName) &&
                anim.GetCurrentAnimatorStateInfo(animLayer).normalizedTime < 1.0f)
            return true;
        else
            return false;
    }
    IEnumerator finalDialogues()
    {
        freezePlayer();

        yield return new WaitForSeconds(2);
        activateDialogueCamera(true);
        finalDialogue.GetComponent<Text>().text = "Wilburr The Lonely Giant: Wow! Did you just turn the sky God off??";
        dialogueSounds[3].Play(0);
        yield return new WaitForSeconds(4);
        finalDialogue.GetComponent<Text>().text = "Phlo: I only stabilized the satellite using this small button here!";
        dialogueSounds[4].Play(0);
        yield return new WaitForSeconds(5);
        finalDialogue.GetComponent<Text>().text = "Wilburr The Lonely Giant: Satellite? So that's what that thing was!I've never heard of a satellite.Thank you for this kind act, stranger. If I can do anything to help you on your adventure, please tell me!";
        dialogueSounds[5].Play(0);
        yield return new WaitForSeconds(13);
       finalDialogue.GetComponent<Text>().text = "Phlo: I could use help getting to Treestan... Is that something you could do for me?";
        dialogueSounds[6].Play(0);
        yield return new WaitForSeconds(6);
       finalDialogue.GetComponent<Text>().text = "Wilburr The Lonely Giant: Of course. That tree has been alive since forever.";
        dialogueSounds[7].Play(0);
        yield return new WaitForSeconds(5);
       finalDialogue.GetComponent<Text>().text = "Phlo: Can you help me find him?";
        dialogueSounds[8].Play(0);
        yield return new WaitForSeconds(4);
       finalDialogue.GetComponent<Text>().text = "Wilburr The Lonely Giant: Oh, sure! I could toss you to it with my giant hands! But be careful and remember, the radioactive rain kills in 5 minutes.";
        dialogueSounds[9].Play(0);
        yield return new WaitForSeconds(11);
       finalDialogue.GetComponent<Text>().text = null;
    }
    void freezePlayer()
    {
        character.GetComponent<BasicBehaviour>().enabled = false;
        character.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionY;
        character.transform.LookAt(wilburrGiant.transform);
    }
    void activateDialogueCamera(bool dialogueCameraActive)
    {
        mainCamera.enabled = !dialogueCameraActive;
        dialogueCamera.enabled = dialogueCameraActive;
    }

}
*/