using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class WilburrTrigger : MonoBehaviour
{
    private GameObject finalZone;
    private GameObject button;
    [SerializeField] GameObject wilburrGiant;
    [SerializeField] GameObject wilburrChase;
    [SerializeField] GameObject buttonText;
    [SerializeField] GameObject phloInHand;
    [SerializeField] GameObject phloInAir;

    private Animator wilburrAnimator;
    private bool inZone = false;
    private bool initiatedMonologue = false;
    public GameObject finalText;
    private GameObject helpText;
    private GameObject finalDialogue;
    private GameObject character;
    [SerializeField] Camera mainCamera;
    [SerializeField] Camera dialogueCamera1;
    [SerializeField] Camera dialogueCamera2;
    [SerializeField] Camera flyingCamera;
    [SerializeField] Animator flyingCameraAnimator;
    [SerializeField] Camera wilburrBackCamera;
    [SerializeField] GameObject startRadioactiveScreen;
    [SerializeField] Animator phloBeingThrownAnimator;
    private Vector3 position;
    AudioSource[] dialogueSounds;
    AudioSource[] satelliteSounds;
    [SerializeField] GameObject radioactiveTrigger;
    void Start()
    {
        dialogueSounds = GetComponents<AudioSource>();
        satelliteSounds = GameObject.Find("Satellite").GetComponents<AudioSource>();
        finalZone = GameObject.Find("FinalZone");
        button = GameObject.Find("Button");
        helpText = GameObject.Find("HelpText");
        finalText = GameObject.Find("FinalText");
        finalDialogue = GameObject.Find("FinalDialogue");
        character = GameObject.Find("Character");
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == finalZone.name) inZone = true;
    }
    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.name == finalZone.name) inZone = false;
    }
    private void Update()
    {
         if(inZone)
        {
            buttonText.GetComponent<Text>().text = "Press F to stabilize the gravity!";
            if(Input.GetButtonDown("Fire1")  && !initiatedMonologue
            && finalText.GetComponent<Text>().text == "")
            {
                buttonText.SetActive(false);
                initiatedMonologue = true;
                wilburrChase.SetActive(false);
                wilburrGiant.SetActive(true);
                wilburrAnimator = wilburrGiant.GetComponent<Animator>();
                StartCoroutine(finalDialogues());
                StartCoroutine(stableSounds());
            

            }
            
                

           
        }

    }
    IEnumerator stableSounds()
    {
        yield return new WaitForSeconds(2);
        satelliteSounds[1].Play();
    }
    IEnumerator finalDialogues()
    {
        character.GetComponent<Animator>().Play("Button Pushing");
        phloInHand.SetActive(false);
        yield return new WaitForSeconds(2);
        freezePlayer();
        yield return new WaitForSeconds(2);
        mainCamera.gameObject.SetActive(false);
        dialogueCamera1.gameObject.SetActive(true);
        finalDialogue.GetComponent<Text>().text = "Wilburr The Lonely Giant: Wow! Did you just turn the sky God off??";
        dialogueSounds[3].Play(0);
        yield return new WaitForSeconds(4);
        dialogueCamera1.gameObject.SetActive(false);
        dialogueCamera2.gameObject.SetActive(true);
        finalDialogue.GetComponent<Text>().text = "Phlo: I only stabilized the satellite using this small button here!";
        dialogueSounds[4].Play(0);
        yield return new WaitForSeconds(5);
        dialogueCamera1.gameObject.SetActive(true);
        dialogueCamera2.gameObject.SetActive(false);
        finalDialogue.GetComponent<Text>().text = "Wilburr The Lonely Giant: Satellite? So that's what that thing was!I've never heard of a satellite.Thank you for this kind act, stranger. If I can do anything to help you on your adventure, please tell me!";
        dialogueSounds[5].Play(0);
        yield return new WaitForSeconds(13);
        dialogueCamera1.gameObject.SetActive(false);
        dialogueCamera2.gameObject.SetActive(true);
        finalDialogue.GetComponent<Text>().text = "Phlo: I could use help getting to Treestan... Is that something you could do for me?";
        dialogueSounds[6].Play(0);
        yield return new WaitForSeconds(6);
        dialogueCamera1.gameObject.SetActive(true);
        dialogueCamera2.gameObject.SetActive(false);
        finalDialogue.GetComponent<Text>().text = "Wilburr The Lonely Giant: Of course. That tree has been alive since forever.";
        dialogueSounds[7].Play(0);
        yield return new WaitForSeconds(5);
        dialogueCamera1.gameObject.SetActive(false);
        dialogueCamera2.gameObject.SetActive(true);
        finalDialogue.GetComponent<Text>().text = "Phlo: Can you help me find him?";
        dialogueSounds[8].Play(0);
        yield return new WaitForSeconds(4);
        dialogueCamera1.gameObject.SetActive(true);
        dialogueCamera2.gameObject.SetActive(false);
        finalDialogue.GetComponent<Text>().text = "Wilburr The Lonely Giant: Oh, sure! I could toss you to it with my giant hands! But be careful and remember, the radioactive rain kills in 5 minutes.";
        dialogueSounds[9].Play(0);
        yield return new WaitForSeconds(11);
        finalDialogue.GetComponent<Text>().text = null;
        yield return new WaitForSeconds(0.3f);
        wilburrAnimator.SetTrigger("WilburrThrow");
        phloInHand.SetActive(true);
        dialogueCamera1.gameObject.SetActive(false);
        wilburrBackCamera.gameObject.SetActive(true);
        yield return new WaitForSeconds(2.3f);
        character.transform.GetChild(0).gameObject.SetActive(false);
        Debug.Log("deaktiviran prvi");
        phloInHand.transform.GetChild(0).gameObject.SetActive(false);
        Debug.Log("deaktiviran drugi");
        phloInAir.SetActive(true);
        wilburrBackCamera.gameObject.SetActive(false);
        flyingCamera.gameObject.SetActive(true);
        yield return new WaitForSeconds(2);
        radioactiveTrigger.SetActive(true);
        

        

    }
    private void freezePlayer()
    {
        gameObject.GetComponent<Animator>().SetFloat("Speed", 0);
        gameObject.GetComponent<BasicBehaviour>().enabled = false;
        mainCamera.GetComponent<ThirdPersonOrbitCamBasic>().enabled = false;
        character.transform.LookAt(wilburrGiant.transform.position);
    }
        private void unfreezePlayer()
    {
        gameObject.GetComponent<Animator>().SetFloat("Speed", 0.2f);
        gameObject.GetComponent<BasicBehaviour>().enabled = true;
        mainCamera.GetComponent<ThirdPersonOrbitCamBasic>().enabled = true;
    }



}
