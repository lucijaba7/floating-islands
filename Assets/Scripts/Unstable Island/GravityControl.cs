using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GravityControl : MonoBehaviour
{

    private int groundedBool;
    public GameObject player;
    public GameObject obstacles1;
    public GameObject obstacles2;
    public GameObject obstacles3;
    Animator gravityAnim1;
    Animator gravityAnim2;
    Animator gravityAnim3;
    [SerializeField] Animator satelliteAnimator;

    [SerializeField] FlyBehaviour flyBehaviour;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Character");
        obstacles1 = GameObject.Find("GravityLevel1");
        obstacles2 = GameObject.Find("GravityLevel2");
        obstacles3 = GameObject.Find("GravityLevel3");
        gravityAnim1 = obstacles1.GetComponent<Animator>();
        gravityAnim2 = obstacles2.GetComponent<Animator>();
        gravityAnim3 = obstacles3.GetComponent<Animator>();

    }


    void OnTriggerEnter(Collider other)
    {
      
        if(other.CompareTag("Player"))
        {
            StartCoroutine(gravitySwitch());
        }
    }

    private IEnumerator gravitySwitch()
    {
        yield return new WaitForSeconds(0.5f);
        gravityAnim1.SetTrigger("switchGravity");
        gravityAnim2.SetTrigger("switchGravity");
        gravityAnim3.SetTrigger("switchGravity");
        satelliteAnimator.SetTrigger("GravitySwitch");
        flyBehaviour.ToggleFly();
    }
}