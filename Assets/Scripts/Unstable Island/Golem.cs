using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Golem : MonoBehaviour
{
 [SerializeField] GameObject giant;
 Animator golemAni;
 public Transform target;
 bool enableAct;
 private void start()
 {
   
     golemAni = GetComponent<Animator>();
     enableAct = true;
 }
 void RotateGolem()
 {
      Vector3 dir = target.position - transform.position;

      transform.localRotation=
        Quaternion.Slerp (transform.localRotation,
            Quaternion.LookRotation(dir),5*Time.deltaTime);
 }
 void MoveGolem()
 {
    if((target.position-transform.position).magnitude>=6)
    {
        bool hasgolemAni = TryGetComponent(out golemAni);
        golemAni.SetBool("Walk",true);
        golemAni.SetBool("Attack", false);
        //transform.Translate(Vector3.forward*golemSpeed*Time.deltaTime,Space.Self); 
      
    }
    
    else if((target.position-transform.position).magnitude<3)
    {
        bool hasgolemAni = TryGetComponent(out golemAni);
        golemAni.SetBool("Walk",false);
        golemAni.SetBool("Attack", true);
    }
 }
 private void Update()
 {  
    giant.GetComponent<NavMeshAgent>().SetDestination(target.position);
    
    
    MoveGolem();
    // RotateGolem();

     if(enableAct==true)
     {
         //RotateGolem();
         MoveGolem();
     }
 }

 void FreezeGolem()
 {
     enableAct=false;
 }
 void UnFreezeGolem()
 {
     enableAct=true;
 }

}