using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;


[RequireComponent(typeof(NavMeshAgent), typeof(Animator))]

public class Enemy : MonoBehaviour
{
    private Animator anim;
    private NavMeshAgent navAgent;

    private Transform following;

    // Start is called before the first frame update
    void Start()
    {
        navAgent = GetComponent<NavMeshAgent>();
        anim = GetComponent<Animator>();   
        following = GameObject.FindWithTag("Player").transform;


    }

    // Update is called once per frame
    void Update()
    {
        navAgent.SetDestination(following.position);
    }

    /*private void OnTriggerEnter(Collider other)
    {

    }

    private void OnTriggerExit(Collider other)
    {

    }*/
}
