using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class StartRadioactiveIsland : MonoBehaviour
{
    [SerializeField] GameObject startScreen;
    [SerializeField] Slider loading;
    void Start()
    {
        StartCoroutine(startRadioactiveIsland());
    }

    private IEnumerator startRadioactiveIsland()
    {
        startScreen.SetActive(true);
        yield return new WaitForSeconds(2f);
        loading.value = 0.2f;
        yield return new WaitForSeconds(2f);
        loading.value = 0.6f;
        yield return new WaitForSeconds(2f);
        loading.value = 1f;
        SceneManager.LoadScene("Radioactive Island 1");
        yield return new WaitForSeconds(0.2f);
        startScreen.SetActive(false);
    }

}
