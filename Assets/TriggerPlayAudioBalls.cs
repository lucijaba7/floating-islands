using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerPlayAudioBalls : MonoBehaviour
{
    GameObject Character;

    void Start()
    {
        Character = GameObject.Find("Character");
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.name.Equals(Character.name))
            {
               gameObject.GetComponent<AudioSource>().Play(0);
            }
    }
}
