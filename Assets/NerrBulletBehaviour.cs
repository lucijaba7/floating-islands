using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NerrBulletBehaviour : MonoBehaviour
{

    [SerializeField] GameObject particle;
    void OnCollisionEnter(Collision co)
    {
        if(co.collider.tag == "Player")
        {
            Instantiate(particle, gameObject.transform.position, Quaternion.identity);
            Destroy(gameObject);
            // newParticle.transform.localScale = sizeOfParticle;
            // StartCoroutine(PhysicalAttack());
            // Zvuk il sta ja znam
        }
        if(co.collider.tag == "worldBarrier")
            Destroy(gameObject);            
    }
    
    void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("worldBarrier"))
            Destroy(gameObject);            
                   
    }
}
