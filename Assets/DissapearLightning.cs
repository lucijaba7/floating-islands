using UnityEngine;

public class DissapearLightning : MonoBehaviour
{

    public float timer = 0.0f;
    public float waitTimeDissapear = 0;

    void Update()
    {
        waitTimeDissapear = Random.Range(2, 5);

        timer += Time.deltaTime;
        if (timer > waitTimeDissapear) DestoryLightning();
    }
    void DestoryLightning()
    {
        Destroy(gameObject);
    }
}
