using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class CaveDialogueCamera3Script : MonoBehaviour
{
    [SerializeField] GameObject ninaWormina;
    // Start is called before the first frame update
    void Start()
    {
        Debug.Log(gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        gameObject.transform.LookAt(ninaWormina.transform.position);
        gameObject.transform.Translate(Vector3.right * 0.001f);
    }
}
