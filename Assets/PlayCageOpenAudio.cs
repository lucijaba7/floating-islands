using UnityEngine;

public class PlayCageOpenAudio : MonoBehaviour
{

    AudioSource audioData;

    void Play()
    {
        audioData = GetComponent<AudioSource>();
        audioData.Play(0);
    }
}
