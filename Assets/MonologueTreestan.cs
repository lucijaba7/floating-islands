using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MonologueTreestan : MonoBehaviour
{
    GameObject StartMonologueZone;
    private bool inZone = false;
    private GameObject monologueText;
    private GameObject dialogueText;
    private GameObject character;
    private GameObject treestanLud;
    private GameObject treestanBarrier;
    private GameObject newSceneTransition;
    private Animator characterAnimator;
    private Animator treestanAnimator;
    private Camera mainCamera;
    [SerializeField] Camera dialogueCamera;
    [SerializeField] Camera throwCamera;
    [SerializeField] GameObject slimeCounter;
    [SerializeField] GameObject slimeTimer;
    public bool monologueInitiated = false;
    public bool moveTowardsTreestan = false;
    public bool startThrowing = false;
    AudioSource[] dialogueSounds;


    void Start()
    {
        dialogueSounds = GetComponents<AudioSource>();

        StartMonologueZone = GameObject.Find("StartMonologueZone");
        monologueText = GameObject.Find("MonologueText");
        dialogueText = GameObject.Find("DialogueText");
        newSceneTransition = GameObject.Find("NewSceneTransition");
        treestanBarrier = GameObject.Find("TreestanBarrier");
        character = GameObject.Find("Character Phlo");
        characterAnimator = character.GetComponent<Animator>();
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == StartMonologueZone.name) inZone = true;
    }
    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.name == StartMonologueZone.name) inZone = false;
    }


    void Update()
    { 
        if (GameObject.Find("TreestanLud") != null)
        {
            treestanLud = GameObject.Find("TreestanLud");
            treestanAnimator = treestanLud.GetComponent<Animator>();

            if (GameObject.Find("AfterRadioactiveRain") != null
                && inZone
                && monologueText.GetComponent<Text>().text == ""
                && characterAnimator.GetBool("Grounded")
                && characterAnimator.GetFloat("Speed") < 0.01f
                && treestanAnimator.GetBool("finishedRising")
                && !monologueInitiated) StartCoroutine(treestanDialogues());

            if (characterAnimator.GetBool("startedFlying"))
            {
                Debug.Log("startedFlying");
                //characterAnimator.SetFloat("Speed", 0f);
                //character.transform.GetChild(0).gameObject.SetActive(true);
                //character.transform.position = Vector3.MoveTowards(character.transform.position, newSceneTransition.transform.position, 0.4f);
                //mainCamera.transform.LookAt(character.transform.transform.position);
                //mainCamera.transform.GetComponent<ThirdPersonOrbitCamBasic>().enabled = false;
                //character.GetComponent<BasicBehaviour>().enabled = false;
                //character.GetComponent<MoveBehaviour>().enabled = false;
            }
            else if(treestanAnimator.GetBool("startThrow"))
            {
                Debug.Log("startThrow");
                //if (character.transform.GetChild(0) != null) character.transform.GetChild(0).gameObject.SetActive(false);
                GameObject.Find("Character Phlo").gameObject.SetActive(false);
            }
            else if (moveTowardsTreestan && characterAnimator.GetBool("startedFlying") == false)
            {
                Debug.Log("movetowardstreestan");
                character.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
                characterAnimator.SetFloat("Speed", 0.12f);
                character.transform.position = Vector3.MoveTowards(character.transform.position, treestanBarrier.transform.position, 0.1f);
            }
            else if(startThrowing)
            {
                Debug.Log("startthrowing");
                startThrow();
            }
        }
        }
    IEnumerator treestanDialogues()
    {
  
        monologueInitiated = true;
        slimeTimer.SetActive(false);
        slimeCounter.SetActive(false);
        freezePlayer(true);
        yield return new WaitForSeconds(1);
        activateDialogueCamera(true);
        dialogueText.GetComponent<Text>().text = "Treestan: You... you did it my friend. You gave this island a new life!";
       dialogueSounds[0].Play(0);
        yield return new WaitForSeconds(6); //Trajanje audio klipa iznad
        dialogueText.GetComponent<Text>().text = "Phlo: You're welcome.";
       dialogueSounds[1].Play(0);
        yield return new WaitForSeconds(2);
        dialogueText.GetComponent<Text>().text = "Treestan: What brought you here?";
       dialogueSounds[2].Play(0);
        yield return new WaitForSeconds(2);
        dialogueText.GetComponent<Text>().text = "Phlo: I am looking for someone that has what I'm looking for. He is carrying a shiny object with himself. Is there any chance you saw him?";
       dialogueSounds[3].Play(0);
        yield return new WaitForSeconds(8);
        dialogueText.GetComponent<Text>().text = "Treestan: I actually think I did! He had something around his neck� something I have never ever seen before.";
       dialogueSounds[4].Play(0);
        yield return new WaitForSeconds(7);
        dialogueText.GetComponent<Text>().text = "Phlo: I need to find him before he does any more harm.";
       dialogueSounds[5].Play(0);
        yield return new WaitForSeconds(3);
        dialogueText.GetComponent<Text>().text = "Treestan:  I can launch you from my branches! He went to the nearest island east!";
       dialogueSounds[6].Play(0);
        yield return new WaitForSeconds(7);
        dialogueText.GetComponent<Text>().text = "Phlo: Please! I would be grateful.";
       dialogueSounds[7].Play(0);
        yield return new WaitForSeconds(6);
        dialogueText.GetComponent<Text>().text = "Treestan: Good luck my friend.";
        dialogueSounds[8].Play(0);
        yield return new WaitForSeconds(4);
        moveTowardsTreestan = true;
        dialogueText.GetComponent<Text>().text = "";

    }
    void freezePlayer(bool freeze)
    {
        character.GetComponent<BasicBehaviour>().enabled = !freeze;
        if (freeze) {
            character.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
            character.transform.LookAt(treestanBarrier.transform);
        }
        else
        {
            character.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
        }
    }
    void activateDialogueCamera(bool dialogueCameraActive)
    {
        mainCamera = GameObject.Find("Main Camera").GetComponent<Camera>();
        mainCamera.enabled = !dialogueCameraActive;
        dialogueCamera.enabled = dialogueCameraActive;
    }

    void startThrow()
    {
        //characterAnimator.SetFloat("Speed", 0f);
        //characterAnimator.SetBool("Grounded", false);
        dialogueCamera.enabled = false;
        throwCamera.enabled = true;
        //GameObject.Find("Phlo - Character and Movement").transform.parent = treestanLud.transform;
        //GameObject.Find("Phlo - Character and Movement").transform.position = new Vector3(0, 0, 0);
        treestanAnimator.SetBool("startThrow", true);
    }
}
