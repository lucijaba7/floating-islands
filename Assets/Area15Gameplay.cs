using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class Area15Gameplay : MonoBehaviour
{
    public IslandShake islandShake;
    private GameObject character;
    private GameObject blackHole;
    private GameObject nerr;
    private Rigidbody nerrRb;
    private Animator blackHoleAnimator;
    private Animator nerrAnimator;
    [SerializeField] BossFight bossFight;
    [SerializeField] GameObject tpCamera;
    public CameraShake cameraShake;
    private bool cameraFollowNerr = false;
    private bool cameraUpZoomBeggining = false;
    public GameObject captions; //Za prikazivanje teksta dijaloga
    AudioSource[] dialogueSounds; //Za puštanje zvukova dijaloga
    [SerializeField] GameObject area15DialogueCamera1;
    [SerializeField] public GameObject area15DialogueCamera2;
    [SerializeField] public GameObject area15DialogueCamera3;
    [SerializeField] GameObject backgroundMusicFight;
    [SerializeField] GameObject backgroundMusic;
    //[SerializeField] public GameObject cameraHolder; //Kamera na Phlou koju moramo isključiti kako bi se prikazivale ostale kamere
    void Update()
    {
        if (cameraFollowNerr)
        {
            if(tpCamera.transform.position.y < nerr.transform.position.y){
                tpCamera.transform.LookAt(nerr.transform.position);
            }
            
        }
        if (cameraUpZoomBeggining)
        {
            Vector3 cameraLook = new Vector3(nerr.transform.position.x, tpCamera.transform.position.y, nerr.transform.position.z);
            tpCamera.transform.LookAt(cameraLook);
            tpCamera.transform.Translate(Vector3.up * 0.02f);
            tpCamera.transform.Translate(Vector3.forward * 0.02f);   
        }
    }

    void Start()
    {
        character = GameObject.Find("Character");
        blackHole = GameObject.Find("BlackHole");
        nerr = GameObject.Find("Nerr");
        blackHoleAnimator = blackHole.GetComponent<Animator>();
        nerrAnimator = nerr.GetComponent<Animator>();
        captions = GameObject.Find("Captions"); //Za prikazivanje teksta dijaloga
        dialogueSounds = GetComponents<AudioSource>();  //Za puštanje zvukova dijaloga
        // nerr.GetComponent<BossEnemy>().enabled = false;
       
        // nerrRb = nerr.GetComponent<Rigidbody>();
        // nerr.transform.position = new Vector3(0f, -10f, 0f);
    }


    void OnTriggerEnter(Collider other)
    {
        //start Nerr entrance animation
        if(other.CompareTag("Player") )
        {
            this.GetComponent<Collider>().enabled = false;
            StartCoroutine(sceneStart());
        }
            
    }

    private IEnumerator sceneStart()
    {
        // Look at player
        Vector3 targetPostition = new Vector3( character.transform.position.x, 
                                        nerr.transform.position.y, 
                                        character.transform.position.z ) ;
        nerr.transform.LookAt(targetPostition);

        // Player se freeza i gleda
        character.transform.LookAt(blackHole.transform.position);
        dialogueSounds[4].Play(0);
        PlayerFreeze(true);
        

        cameraUpZoomBeggining = true;
        yield return new WaitForSeconds(1);
        cameraUpZoomBeggining = false;

        StartCoroutine(shakeCamera(3f, .4f));

        blackHoleAnimator.SetTrigger("open_hole");
        yield return new WaitForSeconds(2.5f);
        cameraFollowNerr = true;
        
        nerrRb = nerr.AddComponent<Rigidbody>();
        nerrRb.AddForce(transform.up * 30f, ForceMode.Impulse);
                
        yield return new WaitForSeconds(2);


        nerrAnimator.SetBool("NerrFalling", true);
     
        // ovisi kolko trea i to
        yield return new WaitForSeconds(4);
        dialogueSounds[5].Play(0);
        cameraFollowNerr = false;
        StartCoroutine(firstInteraction());
        yield return null;
    }

    private IEnumerator shakeCamera(float duration, float magnitude)
    {
            Vector3 originalPos = tpCamera.transform.localPosition;

            float elapsed = 0.0f;

            while (elapsed < duration){
                float x = Random.Range(-1f, 1f) * magnitude;
               
                tpCamera.transform.localPosition = new Vector3(x, originalPos.y, originalPos.z);
                elapsed += Time.deltaTime;
                // before next iter in while loop, wait til it's drawn
                yield return null;
            }

            tpCamera.transform.localPosition = originalPos;
    }

    private void PlayerFreeze(bool freeze)
    {
        character.GetComponent<Animator>().SetFloat("Speed", 0);
        character.GetComponent<MoveBehaviour>().enabled = !freeze;
        tpCamera.GetComponent<ThirdPersonOrbitCamBasic>().enabled = !freeze;
    }

    private IEnumerator firstInteraction(){
        nerr.GetComponent<CapsuleCollider>().enabled = true;
        nerr.GetComponent<BoxCollider>().enabled = true;
        nerrRb.mass = 1000000f;
        Debug.Log("INTERAKCIJAA");
        /*
        --------------------------
        INTERAKCIJA
        ----------------------------
        */
        Debug.Log("area15DialogueCamera1 uključena");
        area15DialogueCamera1.SetActive(true);
        //tpCamera.gameObject.SetActive(false);
        captions.GetComponent<Text>().text = "WHO'S DISTURBING ME, THE SUPREME ONE?!";
        dialogueSounds[0].Play(0);
        yield return new WaitForSeconds(5);

        Debug.Log("area15DialogueCamera2 uključena");
        area15DialogueCamera2.SetActive(true);
        area15DialogueCamera1.SetActive(false);
        captions.GetComponent<Text>().text = "It's me.. I came to take the treasure so I can unite us again, all our islands!";
        dialogueSounds[1].Play(0);
        yield return new WaitForSeconds(5);


        area15DialogueCamera3.SetActive(true);
        area15DialogueCamera2.SetActive(false);
        captions.GetComponent<Text>().text = "Haha! So you came here to die!";
        dialogueSounds[2].Play(0);
        yield return new WaitForSeconds(3);
        captions.GetComponent<Text>().text = "";
        //cameraHolder.gameObject.SetActive(true);

        //Početak BoosFight-a
        yield return new WaitForSeconds(0.5f);
        beginBossFight();
        backgroundMusic.SetActive(false);
        backgroundMusicFight.SetActive(true);
        area15DialogueCamera3.gameObject.SetActive(false);
        yield return null;
    }
    public void TurnOffBgMusic()
    {
        backgroundMusicFight.SetActive(false);
    }

    private void beginBossFight(){
        PlayerFreeze(false);
        bossFight.StartBossFight();
    }


}
