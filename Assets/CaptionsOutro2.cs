using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class CaptionsOutro2 : MonoBehaviour
{
    private GameObject captions; 
    AudioSource[] captionsSounds; //Za puštanje zvukova dijaloga
    [SerializeField] GameObject transitionScreen;
    void Start()
    {
        captions = GameObject.Find("Captions"); //Za prikazivanje teksta dijaloga
        captionsSounds = GetComponents<AudioSource>();
        StartCoroutine(showCaptions2());
    }

    void Update()
    {
        
    }

    private IEnumerator showCaptions2(){
        yield return new WaitForSeconds(2);
        captions.GetComponent<Text>().text = "Phlo: All of the world is now stable, nature is back to its original form and the world prospers once again…";
        captionsSounds[0].Play(0);
        yield return new WaitForSeconds(6);
        captions.GetComponent<Text>().text = "";
        yield return new WaitForSeconds(1);
        captions.GetComponent<Text>().text = "Phlo: Thank you grandpa. This is an adventure I will never forget.";
        captionsSounds[1].Play(0);
        yield return new WaitForSeconds(5);
        transitionScreen.SetActive(true);
        SceneManager.LoadScene("EndCreditsScene");
        yield return new WaitForSeconds(0.5f);
        transitionScreen.SetActive(false);
    }
}
