using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hitting : MonoBehaviour
{
    [SerializeField] Animator characterAnimator;
    [SerializeField] GameObject leftHand;
    [SerializeField] GameObject rightHand;
    [SerializeField] GameObject particle;
    AudioSource [] hittingSounds;
    
    void Start()
    {
        hittingSounds = gameObject.GetComponents<AudioSource>();
    }
    void Update()
    {
        if (Input.GetButtonDown("HitRight")) 
        {
            characterAnimator.SetTrigger("hitRight");
            StartCoroutine(activateHandAttack(rightHand));
            // rightHand.GetComponent<SphereCollider>().SetActive(true);
        }
        if (Input.GetButtonDown("HitLeft")) 
        {
            characterAnimator.SetTrigger("hitLeft");
            StartCoroutine(activateHandAttack(leftHand));
        }
    }

    private IEnumerator activateHandAttack(GameObject hand)
    {
        yield return new WaitForSeconds(0.5f);
        hand.GetComponent<CapsuleCollider>().enabled = true;
        Instantiate(particle, hand.transform.position, Quaternion.identity);
        // Zvuk udarca u pod
        hittingSounds[0].Play(0);
        yield return new WaitForSeconds(0.5f);
        hand.GetComponent<CapsuleCollider>().enabled = false;
    }
}
