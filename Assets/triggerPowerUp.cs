using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class triggerPowerUp : MonoBehaviour
{
    [SerializeField] PhloShoot phloShoot;
    [SerializeField] SphereSpawnerController sphereSpawnerController;
    [SerializeField] int i;
    [SerializeField] public GameObject powerCanvas;
    [SerializeField] BossFight bossFight;

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player"))
        {
            if (!powerCanvas.active)
            { 
                powerCanvas.SetActive(true);
                bossFight.togglePhloShoot();
            }
            phloShoot.fillPowerMagazine();
            sphereSpawnerController.triggerChangeSphere(i);
        }
    }
}
