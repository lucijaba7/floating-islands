using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeactivateHelpBox : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(deactivateHelpBox());
    }
    private IEnumerator deactivateHelpBox()
    {
        yield return new WaitForSeconds(5);
        gameObject.SetActive(false);
    }
}
