using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ChangeScreenOnIntro : MonoBehaviour
{
    [SerializeField] GameObject startScreen;
    [SerializeField] Slider loading;
    private void Start()
    {
        StartCoroutine(startBubblesIsland());
    }

    private IEnumerator startBubblesIsland()
    {
        startScreen.SetActive(true);
        yield return new WaitForSeconds(2f);
        loading.value = 0.2f;
        yield return new WaitForSeconds(2f);
        loading.value = 0.6f;
        yield return new WaitForSeconds(2f);
        loading.value = 1f;
        SceneManager.LoadScene("Bubbles Island");
        yield return new WaitForSeconds(0.1f);
        startScreen.SetActive(false);
    }
}
