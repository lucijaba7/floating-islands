using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TakeDamage : MonoBehaviour
{
    [SerializeField] int health;
    public PhloHealthBar healthBar;
    [SerializeField] BossFight bossFight;
    [SerializeField] Animator nerrAnimator;
    private bool isDead = false;

    // Start is called before the first frame update
    void Update()
    {
        if(isDead){
            StartCoroutine(Die());
        }
        
    }

    void OnCollisionEnter(Collision co)
    {
        
        if(co.collider.tag == "nerrBullet")
        {
            Debug.Log("lupio");
            health -= 5;
            healthBar.SetHealth(health);
            if(health < 1) isDead = true;
        }
            
    }

    private IEnumerator Die(){
        this.GetComponent<Animator>().SetTrigger("die");
        yield return new WaitForSeconds(2);
        resetCharacter();
        bossFight.ResetBossFight();
    }

    public void resetCharacter()
    {
        isDead = false;
        health = 1000;
        healthBar.SetHealth(health);
    }

    public void physicalAttackDamage()
    {   
        if(nerrAnimator.GetCurrentAnimatorStateInfo(0).IsName("Giant@UnarmedAttack01"))
        {
            this.GetComponent<Animator>().SetTrigger("fall");
            StartCoroutine(takeDamageIn1Sec());
        }

    }

    private IEnumerator takeDamageIn1Sec()
    {
        yield return new WaitForSeconds(1);
        health -= 100;
        healthBar.SetHealth(health);
        if(health < 1) isDead = true;
    }

}
