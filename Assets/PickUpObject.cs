using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUpObject : MonoBehaviour
{
    [SerializeField] EndScene endScene;

    void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player") )
        {
            // if(Input.GetKeyDown("F")){
                endScene.pickUp(gameObject);
                StartCoroutine(destroyIn2());
            // }
        }   
    }

    private IEnumerator destroyIn2()
    {
        yield return new WaitForSeconds(1.5f);
        Destroy(gameObject);
    }
}
