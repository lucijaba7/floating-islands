using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerDissapearOnTreestan : MonoBehaviour
{
    private GameObject TreestanBarrier;
    private void Start()
    {
        TreestanBarrier= GameObject.Find("TreestanBarrier");
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == TreestanBarrier.name && gameObject.GetComponent<MonologueTreestan>().monologueInitiated)
        {
            gameObject.transform.GetChild(0).gameObject.SetActive(false);
            gameObject.GetComponent<MonologueTreestan>().moveTowardsTreestan = false;
            gameObject.GetComponent<MonologueTreestan>().startThrowing = true;
        }
    }
}
