using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class CaptionsOutro1 : MonoBehaviour
{
    private GameObject captions; 
    AudioSource[] captionsSounds; //Za puštanje zvukova dijaloga
    void Start()
    {
        captionsSounds = GetComponents<AudioSource>();
        captions = GameObject.Find("Captions"); //Za prikazivanje teksta dijaloga
        StartCoroutine(showCaptions());
    }

    void Update()
    {
        
    }

    private IEnumerator showCaptions(){
        yield return new WaitForSeconds(2);
        captions.GetComponent<Text>().text = "Phlo: So the stories are true. Our planet is finally complete.";
        captionsSounds[0].Play(0);
        yield return new WaitForSeconds(5);
        captions.GetComponent<Text>().text = "";
        yield return new WaitForSeconds(1);
        SceneManager.LoadScene("Outro - Final scene");
    }
}
